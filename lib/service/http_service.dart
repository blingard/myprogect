import 'dart:convert';
import 'dart:io';

import 'package:basic_utils/basic_utils.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/models/agent.dart';
import 'package:gos/models/district.dart';
import 'package:gos/models/doctype.dart';
import 'package:gos/models/documents.dart';
import 'package:gos/models/error_object.dart';
import 'package:gos/models/gps_coordinates.dart';
import 'package:gos/models/sector.dart';
import 'package:gos/models/site.dart';
import 'package:http/http.dart' as http;



class HTTPService{

  //Pour letissia
  //static const API = 'http://192.168.100.84:8051/maincontroller/';
  //static const FileAPI = 'http://192.168.100.84:8051/gosfile/';
  //static const API = 'http://192.168.100.142:8051/maincontroller/';
  //static const FileAPI = 'http://192.168.100.142:8051/gosfile/';


  //for server
  static const API = 'http://www.pagegos.com:8051/maincontroller/';
  static const FileAPI = 'http://www.pagegos.com:8051/gosfile/';



 // static const API = 'http://192.168.100.6:8051/maincontroller/';
  //static const FileAPI = 'http://192.168.100.6:8051/gosfile/';

  //static const API = 'http://127.0.0.1:8051/maincontroller/';
  //static const FileAPI = 'http://127.0.0.1:8051/gosfile/';
  //static const API = 'http://192.168.100.6:8051/maincontroller/';
  //static const FileAPI = 'http://192.168.100.6:8051/gosfile/';
  //static const API = 'http://192.168.8.101:8051/maincontroller/';
  //static const FileAPI = 'http://192.168.8.101:8051/gosfile/';

  static const headers ={
    'Content-Type' : 'application/json',
    "Accept" : 'application/json'
  };

  //************        Agent         **************

  Future<APIResponse<dynamic>> connectAgentWithLogin(Agent agent) async{
    var url = Uri.parse(API+"agents/connectAgentWithLoginAndPass");
    return http.post(url, headers: headers, body:jsonEncode(agent)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200){
          Map<String, dynamic> jsonData = json.decode(data.body);
          Agent createAgent = Agent(
              agentId: jsonData['agentId'],
              agentName: jsonData['agentName'],
              agentLogin: jsonData['agentLogin'],
              agentPassword: jsonData['agentPassword'],
              agentPhoneNumber: jsonData['agentPhoneNumber'],
              agentSectorList: [],
              status: jsonData['status'],
              agentDeviceList: [],
              isFirstConnection: jsonData['isFirstConnection'],
              );
          return APIResponse<Agent>(data: createAgent);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Empty")));
  }

  //**********************************Remember Password**************************************************

  Future<APIResponse<dynamic>> rememberAgentPassword(Agent agent) async{
    var url = Uri.parse(API+"agents/rememberAgentPassword");
    return http.post(url, headers: headers, body:jsonEncode(agent)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200){
        Map<String, dynamic> jsonData = json.decode(data.body);
        Agent createAgent = Agent(
          agentId: jsonData['agentId'],
          agentName: jsonData['agentName'],
          agentLogin: jsonData['agentLogin'],
          agentPassword: jsonData['agentPassword'],
          agentPhoneNumber: jsonData['agentPhoneNumber'],
          agentSectorList: [],
          status: jsonData['status'],
          agentDeviceList: [],
          isFirstConnection: jsonData['isFirstConnection'],
        );
        return APIResponse<Agent>(error: false,errorMessage: "",data: createAgent);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error 404")));
  }


  Future<APIResponse<dynamic>> newAgentIdentifier(Agent agent) async{
    var url = Uri.parse(API+"agents/defineNewIdentifiers");
    return http.post(url, headers: headers, body:jsonEncode(agent)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200){
        Map<String, dynamic> jsonData = json.decode(data.body);
        Agent createAgent = Agent.fromJson(jsonData);
        return APIResponse<Agent>(error: false,errorMessage: "",data: createAgent);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

  Future<APIResponse<dynamic>> verifyLogin(Agent agent) async{
    var url = Uri.parse(API+"verifyLogin");
  return http.post(url, headers: headers, body:jsonEncode(agent)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200){
        Map<String, dynamic> jsonData = json.decode(data.body);
        Agent createAgent = Agent.fromJson(jsonData);
        return APIResponse<Agent>(error: false,errorMessage: "",data: createAgent);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

  //************        Site         **************

  Future<APIResponse<List<dynamic>>> getSites(District district) async {
    var url = Uri.parse(API+"sector/getAllSiteOfDistrict");
    return http.post(url, headers: headers, body:jsonEncode(district)).then((data) {
      if (data.statusCode == 200) {
        if(data.body.length<50){
          return APIResponse<List<ErrorObject>>(error: true, errorMessage: 'Empty',data: []);
        }
        else{
          final jsonData = json.decode(data.body);
          List<Site> sites =[];
          for (var item in jsonData) {
            final District district = District(
              districtId: item['districtId']['districtId'],
              districtName: item['districtId']['districtName'],
              description: item['districtId']['description'],
              status: item['districtId']['status'],
              sectorId: null,
              commune:  item['districtId']['commune'],
              densite: item['districtId']['densite'],
              population: item['districtId']['population'],
              siteList: [],
              standing: item['districtId']['standing'],
            );
            final int siteId = item['siteId'];
            final String refSite = item['refSite'];
            final String ownerCni = item['ownerCni'];
            final String ownerPhone = item['ownerPhone'];
            final String siteName = item['siteName'];
            final String ownerName = item['ownerName'];
            final int status = item['status'];
            final Site site = Site(
                siteId: siteId,
                refSite: refSite,
                ownerCni: ownerCni,
                ownerPhone: ownerPhone,
                ownerName: ownerName,
                siteName: siteName,
                status: status,
                districtId: district,
                documentsList: [],
                gpsCoordinatesList: []
            );
            sites.add(site);
          }
          return APIResponse<List<Site>>(data: sites);
        }

      } else {
        return APIResponse<List<ErrorObject>>(error: true, errorMessage: 'An error occured',data: []);
      }
    })
        .catchError((_) => APIResponse<List<ErrorObject>>(error: true, errorMessage: 'Unable to contact the server', data: []));
  }

  Future<APIResponse<dynamic>> createSite(Site site, int agentId) async{
    Map<String, String> qParams = {
      'agentId': agentId.toString()
    };
    String url = HttpUtils.addQueryParameterToUrl(API+'site/createSiteFlutter', qParams);
    var uri = Uri.parse(url);
    print(site.documentsList[0].docTypeId!.docTypeName);
    return http.post(uri, headers: headers, body:jsonEncode(site)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200 ){
        Map<String, dynamic> jsonData = json.decode(data.body);
        Site createSite = Site(
          siteId: jsonData["siteId"],
          refSite: jsonData["refSite"],
          siteName: jsonData["siteName"],
          ownerName: jsonData["ownerName"],
          ownerPhone: jsonData["ownerPhone"],
          ownerCni: jsonData["ownerCni"],
          status: jsonData["status"],
          districtId: District(
              districtId: jsonData["districtId"]["districtId"],
              districtName: jsonData["districtId"]["sectorName"],
              description: jsonData["districtId"]["description"],
              commune: jsonData["districtId"]["commune"],
              standing: jsonData["districtId"]["standing"],
              population: jsonData["districtId"]["population"],
              densite: jsonData["districtId"]["densite"],
              status: jsonData["districtId"]["status"],
              siteList: [],
              sectorId: null,
          ),
          gpsCoordinatesList: [],
          documentsList: []
        );
        return APIResponse<Site>(error: false,errorMessage: "",data: createSite);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

  //************        Sector         **************
  Future<APIResponse<dynamic>> getAllDistrictSector(Sector sector) async {
    var url = Uri.parse(API+"sector/getAllDistrictOfSector");
    return await http.post(url, headers: headers, body: jsonEncode(sector)).then((data) {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        List<District> districtList=[];
        for(var item in jsonData){
          final District district = District(
              districtId: item["districtId"],
              districtName: item["districtName"],
              description: item["description"],
              commune: item["commune"],
              standing: item["standing"],
              population: item["population"],
              densite: item["densite"],
              status: item["status"],
              siteList: []
          );
          districtList.add(district);
        }
        Util.districtsList = districtList;
        return APIResponse<Sector>(error: false,errorMessage: "",data: sector);
      } else {
        return APIResponse<List<ErrorObject>>(error: true, errorMessage: 'An error occured',data: []);
      }
    })
        .catchError((_) => APIResponse<List<ErrorObject>>(error: true, errorMessage: 'Unable to contact the server',data: []));
  }


  Future<APIResponse<dynamic>> getAgentSectors(Agent agent) async {
    var url = Uri.parse(API+"agents/getAllAgentsSectorsForMobile");
    return http.post(url, headers: headers, body: jsonEncode(agent)).then((data) async {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        Sector sector = Sector(
          sectorId: jsonData['sectorId'],
          sectorName: jsonData['sectorName'],
          description: jsonData['description'],
          status: jsonData['status'],
          agentSectorList: [],
          districtList: []

        );
        await getAllDistrictSector(sector);
        return APIResponse<Sector>(error: false,errorMessage: "",data: sector);
      } else {
        return APIResponse<List<ErrorObject>>(error: true, errorMessage: 'An error occured',data: []);
      }
    })
        .catchError((_) => APIResponse<List<ErrorObject>>(error: true, errorMessage: 'Unable to contact the server',data: []));
  }

  //************        Documents         **************

  Future<APIResponse<dynamic>> getDocuments()  {
    var url = Uri.parse(API+"documents/getAllSiteDocuments");
    return http.get(url).then((data) {

      if (data.statusCode == 200) {
        List<dynamic> body = jsonDecode(data.body);

        List<Documents> documents = body
            .map(
              (dynamic item) => Documents.fromJson(item),
        )
            .toList();
        return APIResponse<List<Documents>>(error: false,errorMessage: "",data: documents);
      } else {
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: 200, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

  Future<APIResponse<dynamic>> createDocument(Documents document, int agentId, String acte) async{
    Map<String, String> qParams = {
      'agentId': agentId.toString(),
      'acte': acte
    };
    String url = HttpUtils.addQueryParameterToUrl(API+'documents/createDocumentsFlutter', qParams);
    var uri = Uri.parse(url);
    return http.post(uri, headers: headers, body:jsonEncode(document)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200 ){
        Map<String, dynamic> jsonData = json.decode(data.body);
        Documents createDocument = Documents(
          documentCode: jsonData["documentCode"],
          documentNumber: jsonData["documentNumber"],
          documentId: jsonData["documentId"],
          documentName: jsonData["documentName"],
          documentPath: jsonData["documentPath"],
          documentExtension: jsonData["documentExtension"],
          createdDate: jsonData["createdDate"],
          status: jsonData["status"],
          docTypeId: DocType(
              docTypeId: jsonData["docTypeId"]["docTypeId"],
              docTypeName: "IMAGE",
              description: null,
              documentsList: null
          ),
          siteId: document.siteId!,
          secretKey: "secretKey",
        );
        return APIResponse<Documents>(data: createDocument);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }


  //************        GPSCoordonates         **************

  Future<APIResponse<dynamic>> getGPSCoordinates(Site site)  {
    var url = Uri.parse(API+"gpsCoordinates/getAllCoordinatesSite");
    return http.post(url, headers: headers, body:jsonEncode(site)).then((data) {
      if (data.statusCode == 200) {
        List<dynamic> jsonData = jsonDecode(data.body);
          List<GpsCoordinates> coordinates=[];
          for(var item in jsonData){
            final GpsCoordinates coordinate = GpsCoordinates(
              gpsCoordId: item["gpsCoordId"],
              pointName: item["pointName"],
              siteId: null,
              xcoord: item["xcoord"],
              ycoord: item["ycoord"],
            );
            coordinates.add(coordinate);

          }
          return APIResponse<List<GpsCoordinates>>(error: false,errorMessage: "",data: coordinates);
      } else {
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: data.statusCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

  Future<APIResponse<dynamic>> createGPSCoordinates(GpsCoordinates coordinates) async{
    var url = Uri.parse(API+"gpsCoordinates/createGpsCoordinates");
    return http.post(url, headers: headers, body:jsonEncode(coordinates)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200){
        Map<String, dynamic> jsonData = json.decode(data.body);
        GpsCoordinates createCoordinates = GpsCoordinates.fromJson(jsonData);
        return APIResponse<GpsCoordinates>(error: false,errorMessage: "",data: createCoordinates);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

  // ************************ Uploads *************************

  Future<APIResponse<dynamic>> uploadImage(File imageFile) async {
    var uri = Uri.parse(FileAPI + 'uploadimage');
    String uploadedImagePath = '';
    var request = http.MultipartRequest('POST', uri)
      ..files.add(await http.MultipartFile.fromPath(
          'file', imageFile.path));
    await request.send().then((result)  async {
      await http.Response.fromStream(result)
          .then((response) {

        //return response.body;
        uploadedImagePath = response.body.substring(19,response.body.indexOf(',')-1);


      });
    }).catchError((err) => print('error : '+err.toString()))
        .whenComplete(()
    {});
    return APIResponse<String>(error: false,errorMessage: "",data: uploadedImagePath);
  }

  Future<APIResponse<dynamic>> uploadVideo(File videoFile) async {
    var uri = Uri.parse(FileAPI + 'uploadvideo');
    String uploadedVideoPath = '';
    var request = http.MultipartRequest('POST', uri)
      ..files.add(await http.MultipartFile.fromPath(
          'file', videoFile.path));
    await request.send().then((result)  async {

      await http.Response.fromStream(result)
          .then((response) {

        if (response.statusCode == 200)
        {
          print("Uploaded! ");
          print('response.body '+response.body);
        }
        print(response.body.substring(19,response.body.indexOf(',')-1));
        //return response.body;
        uploadedVideoPath = response.body.substring(19,response.body.indexOf(',')-1);


      });
    }).catchError((err) => print('error : '+err.toString()))
        .whenComplete(()
    {});
    return APIResponse<String>(error: false,errorMessage: "",data: uploadedVideoPath);
  }

  Future<APIResponse<dynamic>> uploadAudio(File audioFile) async {
    var uri = Uri.parse(FileAPI + 'uploadaudio');
    String uploadedAudioPath = '';
    var request = http.MultipartRequest('POST', uri)
      ..files.add(await http.MultipartFile.fromPath(
          'file', audioFile.path));
    await request.send().then((result)  async {

      await http.Response.fromStream(result)
          .then((response) {

        if (response.statusCode == 200)
        {
          print("Uploaded! ");
          print('response.body '+response.body);
        }
        print(response.body.substring(19,response.body.indexOf(',')-1));
        //return response.body;
        uploadedAudioPath = response.body.substring(19,response.body.indexOf(',')-1);


      });
    }).catchError((err) => print('error : '+err.toString()))
        .whenComplete(()
    {});
    return APIResponse<String>(error: false,errorMessage: "",data: uploadedAudioPath);
  }

  Future<APIResponse<dynamic>> uploadText(File textFile) async {
    var uri = Uri.parse(FileAPI + 'uploadtext');
    String uploadedTextPath = '';
    var request = http.MultipartRequest('POST', uri)
      ..files.add(await http.MultipartFile.fromPath(
          'file', textFile.path));
    await request.send().then((result)  async {

      await http.Response.fromStream(result)
          .then((response) {

        if (response.statusCode == 200)
        {
          print("Uploaded! ");
          print('response.body '+response.body);
        }
        //return response.body;
        uploadedTextPath = response.body.substring(19,response.body.indexOf(',')-1);


      });
    }).catchError((err) => print('error : '+err.toString()))
        .whenComplete(()
    {});
    return APIResponse<String>(error: false,errorMessage: "",data: uploadedTextPath);
  }

  // ********************************* END ****************************************************

  Future<APIResponse<dynamic>> changeAgentPassword(Agent agent, String oldPassword, String newPassword) async{
    Map<String, String> parameter = {
      'newPassword': newPassword,
      'oldPassword' : oldPassword,
    };
    agent.isFirstConnection = false;
    String url = HttpUtils.addQueryParameterToUrl(API+'password/changeAgentPassword', parameter);
    var uri = Uri.parse(url);
    return http.post(uri, headers: headers, body:jsonEncode(agent)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200){
        Map<String, dynamic> jsonData = json.decode(data.body);
        Agent createAgent = Agent(
          agentId: jsonData['agentId'],
          agentName: jsonData['agentName'],
          agentLogin: jsonData['agentLogin'],
          agentPassword: jsonData['agentPassword'],
          agentPhoneNumber: jsonData['agentPhoneNumber'],
          agentSectorList: [],
          status: jsonData['status'],
          agentDeviceList: [],
          isFirstConnection: jsonData['isFirstConnection'],
        );
        return APIResponse<Agent>(error: false,errorMessage: "",data: createAgent);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

  Future<APIResponse<dynamic>> updateAgentInfo(Agent agent) async{
    var url = Uri.parse(API+"agents/updateAgentMobileInfos");
    return http.post(url, headers: headers, body:jsonEncode(agent)).then((data) {
      final int statueCode = data.statusCode;
      if (statueCode == 200){
        Map<String, dynamic> jsonData = json.decode(data.body);
        Agent createAgent = Agent(
          agentId: jsonData['agentId'],
          agentName: jsonData['agentName'],
          agentLogin: jsonData['agentLogin'],
          agentPassword: jsonData['agentPassword'],
          agentPhoneNumber: jsonData['agentPhoneNumber'],
          agentSectorList: [],
          status: jsonData['status'],
          agentDeviceList: [],
          isFirstConnection: jsonData['isFirstConnection'],
        );
        return APIResponse<Agent>(error: false,errorMessage: "",data: createAgent);
      }else{
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: statueCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

  Future<APIResponse<List<dynamic>>> getSiteDocuments(Site site) async {
    var url = Uri.parse(API+"documents/getAllSiteDocuments");
    return http.post(url, headers: headers, body: jsonEncode(site)).then((data) {
      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        //print(data.body);
        List<Documents> documents = <Documents>[];

        for (var item in jsonData) {
          final Documents document = Documents(
              documentCode: item["documentCode"],
              documentNumber: item["documentNumber"],
              createdDate: item['createdDate'],
              secretKey: item['secretKey'],
              documentName: item['documentName'],
              documentPath: item['documentPath'],
              documentId: item['documentId'],
              status: item['status'],
              siteId: site,
              documentExtension: item['documentExtension'],
              docTypeId: new DocType(docTypeId: item['docTypeId']['docTypeId'], docTypeName: item['docTypeId']['docTypeName'], description: item['docTypeId']['description'], documentsList: item['docTypeId']['documentsList'])
          );
          print(document.docTypeId!.docTypeName);
          print(data.body);
          if(document.docTypeId!.docTypeName=='IMAGE'){
            documents.add(document);
          }

        }
        return APIResponse<List<Documents>>(data: documents);
      } else {
        return APIResponse<List<ErrorObject>>(error: true, errorMessage: 'An error occured',data: []);
      }
    })
        .catchError((_) => APIResponse<List<ErrorObject>>(error: true, errorMessage: 'Unable to contact the server',data: []));
  }

  //************        Documents         **************

  Future<APIResponse<dynamic>> getDocTypes()  {
    var url = Uri.parse(API+"documents/getAllDocTypes");
    return http.get(url).then((data) {

      if (data.statusCode == 200) {
        final jsonData = json.decode(data.body);
        List<DocType> documentType = <DocType>[];
        for (var item in jsonData) {
          final DocType docType = DocType(
            docTypeId: item['docTypeId'],
            docTypeName: item['docTypeName'],
            description: item['description'],
            documentsList: [],
          );
          documentType.add(docType);
        }
        return APIResponse<List<DocType>>(error: false,errorMessage: "",data: documentType);
      } else {
        return APIResponse<ErrorObject>(error: true, errorMessage: 'An error occured',data: ErrorObject(errorCode: data.statusCode, errorText: "Error"));
      }
    })
        .catchError((_) => APIResponse<ErrorObject>(error: true, errorMessage: 'Unable to contact the server',data: ErrorObject(errorCode: 404, errorText: "Error")));
  }

}
