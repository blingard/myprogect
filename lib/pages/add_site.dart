
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/models/agent.dart';
import 'package:gos/pages/agent_info.dart';
import 'package:gos/pages/home_drawer.dart';
import 'package:gos/pages/stepper_body.dart';

class AddSitePage extends StatefulWidget{
  final Agent agent;

  AddSitePage({required this.agent});

  @override
  _AddSitePageState createState() => _AddSitePageState ();
  
}

class _AddSitePageState extends State<AddSitePage> {
  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    //EasyLoading.dismiss();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        title: Text('PAGEGOS'),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
        ],

      ),
      drawer: HomeDrawer(),
      body: new StepperBody(),
    );
  }

}

