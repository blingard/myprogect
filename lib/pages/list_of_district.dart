import 'package:flutter/material.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/helper/searchdistrict.dart';
import 'package:gos/pages/district_detail.dart';
import 'package:gos/pages/home_drawer.dart';
import 'package:gos/helper/util.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DistrictList extends StatefulWidget {
  const DistrictList({Key? key}) : super(key: key);

  @override
  _DistrictListState createState() => _DistrictListState();
}

class _DistrictListState extends State<DistrictList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.district),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
          const SizedBox(width: 12),
        ],
      ),
      drawer: HomeDrawer(),
      body:ListView.builder(
        itemCount: Util.districtsList!.length,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Center(
                child: Text(
                  Util.districtsList!.elementAt(index).districtName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20
                  ),
                )
            ),
            subtitle: Container(
              child: Column(
                children: <Widget>[
                  Text(AppLocalizations.of(context)!.commune+": "+ Util.districtsList!.elementAt(index).commune),
                  Text(AppLocalizations.of(context)!.sector+": "+ Util.sectorAgent!.sectorName),
                ],
              ),
            ),
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => DistrictDetail(district: Util.districtsList!.elementAt(index), )
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search, color: Colors.black,),
        onPressed: (){
          showSearch(
            context: context,
            delegate: DistrictSearch(Util.districtsList!),
          );
        },
      ),
    );
  }
}
