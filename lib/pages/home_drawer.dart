import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/pages/add_site.dart';
import 'package:gos/pages/agent_info.dart';
import 'package:gos/pages/list_of_district.dart';
import 'package:gos/pages/login.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomeDrawer extends StatelessWidget{


  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    return Drawer(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: theme.primaryColor,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Container(
                      child: Row(
                        children: [
                          Icon(Icons.menu),
                          Text(
                            'MENU',
                            style: textTheme.headline6,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Divider(
                  height: 1,
                  thickness: 1,
                ),
                ListTile(
                  title: Text(
                    AppLocalizations.of(context)!.draweradd.toUpperCase(),
                    style: TextStyle(fontSize: 18),
                  ),
                  leading: Icon(Icons.add_box),
                  onTap: () {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AddSitePage(agent: Util.connectedAgent!)),
                    );
                  },
                ),
                ListTile(
                  title: Text(
                    AppLocalizations.of(context)!.district.toUpperCase(),
                    style: TextStyle(fontSize: 18),
                  ),
                  leading: Icon(Icons.add_circle_sharp),
                  onTap: () async {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>DistrictList()),
                    );
                  },
                  //selected: _selectedDestination == 1,
                ),
                ListTile(
                  leading: Icon(Icons.account_circle_rounded),
                  title: Text(AppLocalizations.of(context)!.profile.toUpperCase(),style: TextStyle(fontSize: 18),),
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AgentInfo()),
                    );
                  },
                ),
                ListTile(
                  leading: Icon(Icons.settings),
                  title: Text('SETTING'.toUpperCase(),style: TextStyle(fontSize: 18),),

                  //onTap: () => selectDestination(2),
                ),
                ListTile(
                  leading: Icon(Icons.logout),
                  title: Text(AppLocalizations.of(context)!.drawerlogout.toUpperCase(),style: TextStyle(fontSize: 18),),
                  onTap: () {

                    Navigator.of(context)
                        .pushAndRemoveUntil(
                      CupertinoPageRoute(
                          builder: (context) => MyHomePage()
                      ),
                          (_) => false,
                    );
                  },
                ),
              ],
            ),
          ),
          Container(
            // This align moves the children to the bottom
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  // This container holds all the children that will be aligned
                  // on the bottom and should not scroll with the above ListView
                  child: Container(
                      child: Column(
                        children: <Widget>[
                          Divider(),
                          ListTile(
                              //leading: Icon(Icons.copyright),
                              title: Row(
                                children: [
                                  CircleAvatar(
                                    radius: 20,
                                    backgroundImage: AssetImage('assets/icons/logocuy.png'),
                                  ),
                                  SizedBox(width: 10,),
                                  Text('©SECEL'),
                                ],
                              ))
                        ],
                      )
                  )
              )
          )
        ],
      ),
    );




  }

}