import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/helper/searchsite.dart';
import 'package:gos/models/site.dart';
import 'package:gos/pages/home_drawer.dart';
import 'package:gos/pages/site_detail.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ListOfAgentSitePage extends StatefulWidget{
  List<Site> sites;
  ListOfAgentSitePage({required this.sites});
  @override
  _ListOfAgentSitePageState createState() => _ListOfAgentSitePageState();
  
}

class _ListOfAgentSitePageState extends State<ListOfAgentSitePage>{

  late List<Site> _apiResponse;

  @override
  void initState() {
    EasyLoading.dismiss();
    super.initState();
    _apiResponse =widget.sites;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_apiResponse[0].districtId.districtName.toUpperCase()+ ": "+AppLocalizations.of(context)!.sitedetailtitle ),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
          const SizedBox(width: 12),
        ],
      ),
      body: ListView.builder(
        itemCount: _apiResponse.length,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Center(
                child: Text(
                  _apiResponse.elementAt(index).siteName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20
                  ),
                )
            ),
            subtitle: Container(
              child: Column(
                children: <Widget>[
                  Text(AppLocalizations.of(context)!.siteowner +": "+ _apiResponse.elementAt(index).ownerName),
                  Text(AppLocalizations.of(context)!.sectorname +": "+ _apiResponse.elementAt(index).districtId.districtName),
                ],
              ),
            ),
            tileColor: _apiResponse.elementAt(index).status == 0 ? Colors.green : Colors.deepOrangeAccent,
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => SiteDetailPage(
                    site: _apiResponse.elementAt(index),
                  )
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search, color: Colors.black,),
        onPressed: (){
          showSearch(
            context: context,
            delegate: NameSearch(_apiResponse),
          );
        },
      ),
      /*Container(
        child: ListView(
                    children: _apiResponse
                        .map(
                          (Site sites) => ListTile(
                            title: Center(
                              child: Text(sites.siteName,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 20
                                ),
                              ),
                            ),
                            subtitle: Container(
                              child: Column(
                              children: <Widget>[
                                Text(AppLocalizations.of(context)!.siteowner +": "+ sites.ownerName),
                                Text(AppLocalizations.of(context)!.sectorname +": "+ sites.sectorId.sectorName),
                              ],
                          ),
                            ),
                            tileColor: sites.status == 0 ? Colors.green : Colors.deepOrangeAccent,
                        onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (context) => SiteDetailPage(
                                site: sites,
                              )
                          ),
                        ),
                      ),
                    )
                        .toList(),
                  ),
      ),*/
    );
  }
}