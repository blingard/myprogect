import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gos/service/http_service.dart';

class MyBulletList extends StatelessWidget{
  final String? icon;
  final String? path;

  const MyBulletList({Key? key, this.icon, this.path}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    if(icon=="IMAGE"){
      return new Container(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: Container(
          height: 40.0,
          width: 40.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            shape: BoxShape.circle,
            image:  DecorationImage(
                image: NetworkImage(
                    HTTPService.FileAPI+"download?fileKey="+path!,
                ), fit: BoxFit.cover),
          ),
        ),
      );
    }else if(icon=="VIDEO"){
      return new Container(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: Container(
          height: 40.0,
          width: 40.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            shape: BoxShape.circle,
            /*image:  DecorationImage(
                image: AssetImage(
                  "assets/icons/video.jpg"
                ), fit: BoxFit.cover),*/
          ),
        ),
      );
    }
    else if(icon=="TEXT"){
      return new Container(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: Container(
          height: 40.0,
          width: 40.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            shape: BoxShape.circle,
            /*image:  DecorationImage(
                image: AssetImage(
                    "assets/icons/file.jpg"
                ), fit: BoxFit.cover),*/
          ),
        ),
      );
    }
    else if(icon=="AUDIO"){
      return new Container(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: Container(
          height: 40.0,
          width: 40.0,
          decoration: new BoxDecoration(
            color: Colors.transparent,
            shape: BoxShape.circle,
            /*image:  DecorationImage(
                image: AssetImage(
                    "assets/icons/audio.png"
                ), fit: BoxFit.cover),*/
          ),
        ),
      );
    }
    else {
      return new Container(
        padding: EdgeInsets.fromLTRB(25.0, 0.0, 0.0, 0.0),
        child: Container(
          height: 30.0,
          width: 10.0,
          decoration: new BoxDecoration(
            color: Colors.black,
            shape: BoxShape.circle,
          ),
        ),
      );
    }

  }

}