import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/constants.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/models/documents.dart';
import 'package:gos/models/gps_coordinates.dart';
import 'package:gos/models/site.dart';
import 'package:gos/pages/add_document.dart';
import 'package:gos/pages/bullet_list.dart';
import 'package:gos/pages/map.dart';
import 'package:gos/pages/see_last_document.dart';
import 'package:gos/service/http_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SiteDetailPage extends StatefulWidget{
  final Site site;
  SiteDetailPage({required this.site});

  @override
  _SiteDetailPageState createState() => _SiteDetailPageState();
}

class _SiteDetailPageState extends State<SiteDetailPage> {


  String errorMessage = "";
  late Site _site;
  late Documents _doc;

  @override
  void initState() {

    super.initState();
    _site = widget.site;
    //EasyLoading.dismiss();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(AppLocalizations.of(context)!.sitedetailtitle),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
          const SizedBox(width: 12),
        ],
      ),
      body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    child: Text(
                      AppLocalizations.of(context)!.sitedetail,
                      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0,
                          color: Colors.blue),
                    ),
                  ),
                  Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        ListTile(
                          title: Text(AppLocalizations.of(context)!.siteref, style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                          ),
                          subtitle: Text(widget.site.refSite, style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                          ),
                        ),
                        ListTile(
                          title: Text(AppLocalizations.of(context)!.sitename, style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                          ),
                          subtitle: Text(widget.site.siteName, style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                          ),
                        ),
                        ListTile(
                          title: Text(AppLocalizations.of(context)!.siteowner, style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                          ),
                          subtitle: Text(widget.site.ownerName, style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                          ),
                        ),
                        ListTile(
                          title: Text(AppLocalizations.of(context)!.sectorname, style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                          ),
                          subtitle: Text(widget.site.districtId.districtName, style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                          ),
                        ),
                        Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                alignment: Alignment.centerLeft,
                                child:  Column(
                                  crossAxisAlignment: CrossAxisAlignment.stretch,
                                    children: <Widget> [
                                      FlatButton(
                                        onPressed: () async {
                                          APIResponse<dynamic> response;
                                          //EasyLoading.show(status: AppLocalizations.of(context)!.wait);
                                          response = await locator.get<HTTPService>().getSiteDocuments(_site);

                                          if (response.error == false){
                                            List<Documents> documents = response.data;
                                            print(documents.length);
                                            if(documents.isEmpty) {
                                              EasyLoading.showInfo(AppLocalizations.of(context)!.vide, duration: duree);
                                            }else{


                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(builder: (context) => LastDocument(document: documents[documents.length-1],)),
                                              );
                                            }
                                          }
                                        },
                                        color: Colors.blueAccent,
                                        padding: EdgeInsets.all(10.0),
                                        child: Center(
                                          child: Row( // Replace with a Row for horizontal icon + text
                                            children: <Widget>[
                                              Center(child: Icon(Icons.file_present)),
                                              Center(
                                                child: Text(
                                                  AppLocalizations.of(context)!.sitedoc,
                                                  style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.black,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 6.0,),
                                      FlatButton(
                                        onPressed: () async {
                                          EasyLoading.show(status: AppLocalizations.of(context)!.wait);
                                          APIResponse<dynamic> response;
                                          response = await locator.get<HTTPService>().getGPSCoordinates(_site);
                                          if (response.error == false){
                                            List<GpsCoordinates> coordinates = response.data;
                                            if(coordinates.isEmpty) {
                                              EasyLoading.dismiss();
                                              ScaffoldMessenger.of(context).showSnackBar(
                                                SnackBar(
                                                  /*action: SnackBarAction(
                                                    label: 'Action',
                                                    onPressed: () {
                                                      // Code to execute.
                                                    },
                                                  ),*/
                                                  content: const Text('Pas de coordonnées pour ce site'),
                                                  duration: duree,
                                                  width: 280.0, // Width of the SnackBar.
                                                  padding: const EdgeInsets.symmetric(
                                                    horizontal: 8.0, // Inner padding for SnackBar content.
                                                  ),
                                                  behavior: SnackBarBehavior.floating,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(10.0),
                                                  ),
                                                ),
                                              );
                                          }else{
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(builder: (context) => MapMobile(coordinates: coordinates,siteName: _site.siteName,)),
                                              );
                                            }
                                          }
                                        },
                                        color: Colors.blueAccent,
                                        padding: EdgeInsets.all(10.0),
                                        child: Center(
                                          child: Row( // Replace with a Row for horizontal icon + text
                                            children: <Widget>[
                                              Icon(Icons.map_outlined),
                                              Text(
                                                AppLocalizations.of(context)!.siteview,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 6.0,),
                                      FlatButton(
                                        onPressed: () async {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(builder: (context) => AddDocument(
                                              site: widget.site
                                            )),
                                          );
                                        },
                                        color: Colors.blueAccent,
                                        padding: EdgeInsets.all(10.0),
                                        child: Center(
                                          child: Row( // Replace with a Row for horizontal icon + text
                                            children: <Widget>[
                                              Icon(Icons.add),
                                              Text(
                                                AppLocalizations.of(context)!.adddocadd,
                                                style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
    );
  }
}