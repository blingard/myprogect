import 'dart:io';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:gos/components/add_point.dart';
import 'package:gos/components/point_delete.dart';
import 'package:gos/constants.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/models/agent.dart';
import 'package:gos/models/district.dart';
import 'package:gos/models/doctype.dart';
import 'package:gos/models/documents.dart';
import 'package:gos/models/gps_coordinates.dart';
import 'package:gos/models/sector.dart';
import 'package:gos/models/site.dart';
import 'package:gos/pages/list_of_agent_site.dart';
import 'package:gos/service/http_service.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:location/location.dart';

class StepperBody extends StatefulWidget {
  @override
  _StepperBodyState createState() => new _StepperBodyState();
}

class _StepperBodyState extends State<StepperBody> {
//*****************************************insert image******************************************
  Location location = new  Location();
  late bool _serviceEnable;
  late PermissionStatus _permissionGranted;
  late LocationData _locationData;
  bool _isGetLocation=false;
  File? _image = null;
  final picker = ImagePicker();
  List<GpsCoordinates> registeredCoordinates =[];
  List<Documents> sitesImagesList = <Documents>[];
  late int _ratingController;
  String _selectedDistrict="";
  List<String> liste = ['init'];

  Future getImage() async{
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
  String verifPhone(String value){
    bool verif = false;
    String? result;
    int a = 0;
    try {
      int.parse(value);
      verif = true;
    } catch (e) {
      verif = false;
    }
    if(verif==true){
      if (value.isEmpty){
        result = 'Please enter the owner phone';
      }else{
        if(value.length < 1){
          result = 'Please owner phone must have exactly 9 digits';
        }else if(value.length > 9){
          result = 'Please owner phone must have exactly 9 digits';
        }
      }
    }else{
      result = 'ok';
    }
    return result!;
  }

//****************************Geolocalisation stepper******************************

  late double xcoord;
  late double ycoord;

  final TextEditingController _locationNameController = TextEditingController();

  //_StepperBodyState();

//****************************************site creation stepper**************************************
  late String sectorValue;
  bool isLoading = true;
  String errorMessage = "";
  Agent agent = Util.connectedAgent!;
  bool _isButtonEnabled = true;

  late APIResponse<dynamic> _apiResponse;
  late APIResponse<dynamic> _apiResponseSector;
  getDropDownList() async {
    setState(() {
      isLoading = true;
    });
    APIResponse<dynamic> response;
    response = await locator.get<HTTPService>().getAgentSectors(agent);
    setState(() {
      _apiResponseSector = response;
      if(_apiResponseSector.error==false){
        //sector = _apiResponseSector.data;
        print("ne pas griser le btn add site");
      }else{
        print("griser le btn add site");
      }
    });

    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    for(District d in Util.districtsList!){
      liste.add(d.districtName);
    }
    liste.removeWhere((element) => element=='init');
  }

  int _currentStep = 0;
  static var _focusNode = new FocusNode();
  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  static MyData data = new MyData();


  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    void showSnackBarMessage(String message,
        [MaterialColor color = Colors.red]) {
      Scaffold
          .of(context)
          .showSnackBar(new SnackBar(content: new Text(message)));
    }
    Future<bool> _submitDetails() async {
      final FormState formState = _formKey.currentState!;
      bool status = false;
      if (!formState.validate()) {
        status = false;
        EasyLoading.showError(AppLocalizations.of(context)!.stepperphoneerror3, duration: duree);
      }
      else {
        if(registeredCoordinates.length==0){
          status = false;////
          EasyLoading.showError(AppLocalizations.of(context)!.stepperphoneerror3, duration: duree);
        }else{
          status=true;
          formState.save();
          EasyLoading.show(status: AppLocalizations.of(context)!.sitename + " : " + data.siteNameController+ "\n" +
              AppLocalizations.of(context)!.siteref + " : " + data.siteReferenceController+ "\n" +
              AppLocalizations.of(context)!.siteowner + " : " + data.ownerNameController+ "\n" +
              AppLocalizations.of(context)!.siteownerphone + " : " + data.ownerPhoneController+ "\n" +
              AppLocalizations.of(context)!.siteownercni + " : " + data.ownerCNIController+ "\n" +
              AppLocalizations.of(context)!.sector + " : " + data.districtController.districtName

          );
        }
      }
      return status;
    }


    void submitFunction() async{
      await locator.get<HTTPService>().getDocTypes();
      if(_isButtonEnabled == false){

      }else{
        bool state = await _submitDetails();
        APIResponse<dynamic> response;
        if(state) {
          setState(() {
            _isButtonEnabled = false;
          });
          APIResponse<dynamic> responseUploadMessage =  await locator.get<HTTPService>().uploadImage(_image!);
          if (responseUploadMessage.error == false) {
            String pathFile = responseUploadMessage.data;
            responseUploadMessage = await locator.get<HTTPService>().getDocTypes();
            List <DocType> docTypes = responseUploadMessage.data;
            int i = docTypes.indexWhere((element) => element.docTypeName=="IMAGE");
            DocType docType= docTypes[i];
            Documents doc = new Documents(
                documentNumber: "",
                documentCode: "",
                documentPath: pathFile,
                siteId: null,
                documentExtension: '',
                secretKey: '',
                status: null,
                createdDate: '',
                documentName: data.fileNameController,
                documentId: null,
                docTypeId: docType
            );
            sitesImagesList=[];
            sitesImagesList.add(doc);
            registeredCoordinates.add(registeredCoordinates[0]);
            Site newSite = new Site(
                refSite: data.siteReferenceController,
                siteName: data.siteNameController,
                ownerName: data.ownerNameController,
                ownerPhone: data.ownerPhoneController,
                ownerCni: data.ownerCNIController,
                documentsList: sitesImagesList,
                gpsCoordinatesList:registeredCoordinates,
                districtId: data.districtController,
                siteId: null,
                status: 0
            );
            response = await locator.get<HTTPService>().createSite(newSite, Util.connectedAgent!.agentId!);
            APIResponse<dynamic> responseSite = await locator.get<HTTPService>().getSites(data.districtController) as APIResponse<List<Site>>;
            if(responseSite.error == false){
              EasyLoading.showSuccess("", duration: duree);
              _formKey.currentState!.reset();
              _image=null;
              registeredCoordinates = [];
              setState(() {
                _isButtonEnabled = true;
              });
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ListOfAgentSitePage(sites: responseSite.data,)),
              );
            }else{
              setState(() {
                errorMessage = _apiResponse.errorMessage!;
              });
              setState(() {
                _isButtonEnabled = true;
              });
            }
          }
        }
      }


    }




    return Scaffold(
      body: Container(
          child: new Form(
            key: _formKey,
            child: new ListView(children: <Widget>[
              new Stepper(
                physics: ClampingScrollPhysics(),
                steps: _stepper(),
                type: StepperType.vertical,
                currentStep: this._currentStep,
                onStepTapped: (step) {
                  setState(() {
                    this._currentStep = step;
                    if(this._currentStep == 1){
                      this._isGetLocation = false;
                    }
                  });

                },
                onStepContinue: () {
                  setState(() {
                    if(this._currentStep < this._stepper().length - 1){
                      this._currentStep = this._currentStep + 1;
                      setState(() {
                        this._isGetLocation = false;
                      });
                    }else{
                      submitFunction();
                    }
                  });
                },
                onStepCancel: () {
                  setState(() {
                    if(this._currentStep > 0){
                      this._currentStep = this._currentStep - 1;
                    }else{
                      this._currentStep = 0;
                    }
                    setState(() {
                      this._isGetLocation = false;
                    });
                  });
                },
              ),
            ]),
          )
      ),
        floatingActionButton: FloatingActionButton.extended(
          foregroundColor: Colors.black,
          onPressed: _isButtonEnabled ? submitFunction : null,
          backgroundColor: Colors.blue,
          icon: Icon(Icons.add),
          label: Text(
            AppLocalizations.of(context)!.btnaddsite,
            style: new TextStyle(color: Colors.white),
          ),
        ),

    );
  }
  List<Step> _stepper(){
    List<Step> _steps = [
      Step(
          title: Text(AppLocalizations.of(context)!.steppertitle),
          content: Column(
            children: <Widget>[
              SizedBox(height: 12.0),
              TextFormField(
                //focusNode:_focusNode,
                //autofocus: true,
                keyboardType: TextInputType.text,
                autocorrect: false,
                onChanged: (value){
                  setState(() {
                    data.siteReferenceController = value;
                  });
                },
                maxLines: 1,
                validator: (value) {
                  if (value!.isEmpty || value.length < 1){
                    return AppLocalizations.of(context)!.stepperphoneerror4;
                  }
                },
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),
                  labelText: AppLocalizations.of(context)!.siteref,
                  filled: true,
                  isDense: true,
                ),
              ),
              SizedBox(height: 12.0),
              TextFormField(
                //focusNode:_focusNode,
                keyboardType: TextInputType.text,
                autocorrect: false,
                onChanged: (value){
                  setState(() {
                    data.siteNameController = value;
                  });
                },
                maxLines: 1,
                validator: (value) {
                  if (value!.isEmpty || value.length < 1){
                    return AppLocalizations.of(context)!.stepperphoneerror5;
                  }
                },
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),
                  labelText: AppLocalizations.of(context)!.sitename,
                  filled: true,
                  isDense: true,
                ),
              ),
              SizedBox(height: 12.0),
              TextFormField(
                //focusNode:_focusNode,
                keyboardType: TextInputType.text,
                autocorrect: false,
                onChanged: (String value){
                  data.ownerNameController = value;
                },
                maxLines: 1,
                validator: (value) {
                  if (value!.isEmpty || value.length < 1){
                    return AppLocalizations.of(context)!.stepperphoneerror6;
                  }
                },
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),
                  labelText: AppLocalizations.of(context)!.siteowner,
                  filled: true,
                  isDense: true,
                ),
              ),
              SizedBox(height: 12.0),
              TextFormField(
                //focusNode:_focusNode,
                keyboardType: TextInputType.phone,
                autocorrect: false,
                onChanged: (value){
                  setState(() {
                    data.ownerPhoneController = value;
                  });

                },
                maxLines: 1,
                validator: (value) {
                  try {
                    int.parse(value!);
                    if (value.isEmpty){
                      return AppLocalizations.of(context)!.stepperphoneerror2;
                    }else{
                      if(value.length < 1){
                        return AppLocalizations.of(context)!.stepperphoneerror2;
                      }else if(value.length > 9){
                        return AppLocalizations.of(context)!.stepperphoneerror2;
                      }
                    }
                  } catch (e) {
                    return AppLocalizations.of(context)!.stepperphoneerror1;
                  }
                },
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),
                  labelText: AppLocalizations.of(context)!.siteownerphone,
                  filled: true,
                  isDense: true,
                ),
              ),
              SizedBox(height: 12.0),
              TextFormField(
               // focusNode:_focusNode,
                keyboardType: TextInputType.phone,
                autocorrect: false,
                onChanged: (String value){
                  setState(() {
                    data.ownerCNIController = value;
                  });
                },
                maxLines: 1,
                validator: (value) {
                  if (value!.isEmpty || value.length < 1){
                    return AppLocalizations.of(context)!.stepperphoneerror7;
                  }
                },
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),
                  labelText: AppLocalizations.of(context)!.siteownercni,
                  filled: true,
                  isDense: true,
                ),
              ),
              SizedBox(height: 12.0),
              DropdownSearch<String>(
                mode: Mode.BOTTOM_SHEET,
                //showSelectedItem: true,
                items: liste,
                label: Util.sectorAgent!.sectorName+" " + AppLocalizations.of(context)!.district,
                //popupItemDisabled: (String s) => s.startsWith(''),
                onChanged: (a){
                  setState(() {
                    data.districtController = Util.districtsList!.firstWhere((element) => element.districtName==a!);
                  });
                },
                validator: (value){
                  if (value!.isEmpty || value.length < 1){
                    return AppLocalizations.of(context)!.stepperphoneerror7;
                  }
                },
              ),
              SizedBox(height: 12.0,)
            ],
          ),
          isActive: _currentStep >= 0,
          state: StepState.indexed
      ),
      Step(
        state: StepState.indexed,
        title: Text(AppLocalizations.of(context)!.steppercoordinatereport),
        content: Column(
          children: <Widget>[
            ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemBuilder: (_, index) {
                  return ListTile(
                    title: Text(AppLocalizations.of(context)!.stepperrefcoordinate+
                        registeredCoordinates[index].pointName!,
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    subtitle: Text('XCoord='+
                        registeredCoordinates[index].xcoord.toString()+' YCoord='+registeredCoordinates[index].ycoord.toString(),
                    ),
                    trailing: IconButton(
                        onPressed: (){
                          EasyLoading.show(status: "");
                          setState(() {
                            registeredCoordinates.removeAt(index);
                          });
                          EasyLoading.dismiss();
                        },
                        icon: Icon(
                          Icons.delete_forever_sharp,
                          color: Colors.red,
                          size: 30.0,
                        )
                    ),
                  );
                },
                itemCount: registeredCoordinates.length,
              ),

            ElevatedButton(onPressed: () async{
              EasyLoading.show(status: "");
              _serviceEnable = await location.serviceEnabled();
              if(!_serviceEnable){
                _serviceEnable = await location.requestService();
                if(_serviceEnable) return;
              }

              _permissionGranted = await location.hasPermission();
              if(_permissionGranted == PermissionStatus.denied){
                _permissionGranted = await location.requestPermission();
                if(_permissionGranted != PermissionStatus.granted) return;
              }
              _locationData = await location.getLocation();
              setState(() {
                _isGetLocation = true;
                registeredCoordinates.add(GpsCoordinates(xcoord: _locationData.longitude!, ycoord: _locationData.latitude! , pointName: "A"+registeredCoordinates.length.toString()));

              });
              EasyLoading.dismiss();
            }, child: Text('Get Location')),
            _isGetLocation ? Text('Location: ${_locationData.latitude}/${_locationData.longitude}') : Container(),
            /*Builder(
              builder: (_) {
                return ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (_, index) {
                    return Dismissible(
                      key: ValueKey(registeredCoordinates[index].pointName),
                      direction: DismissDirection.startToEnd,
                      onDismissed: (direction) {},
                      confirmDismiss: (direction) async {
                        final result = await showDialog(
                            context: context, builder: (_) => PointDelete());
                        return result;
                      },
                      background: Container(
                        color: Colors.red,
                        padding: EdgeInsets.only(left: 16),
                        child: Align(
                          child: Icon(Icons.delete, color: Colors.white),
                          alignment: Alignment.centerLeft,
                        ),
                      ),
                      child: ListTile(
                        title: Text(AppLocalizations.of(context)!.stepperrefcoordinate+
                          registeredCoordinates[index].pointName!,
                          style: TextStyle(color: Theme.of(context).primaryColor),
                        ),
                        subtitle: Text('XCoord='+
                            registeredCoordinates[index].xcoord.toString()+' YCoord='+registeredCoordinates[index].ycoord.toString(),
                        ),
                      ),
                    );
                  },
                  itemCount: registeredCoordinates.length,
                );
              },
            ),
            Row(
              children: <Widget>[
                Container(
                  height: 40.0,
                  width: 100.0,
                  child:  Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: Colors.transparent,
                    color: Colors.grey,
                    elevation: 10.0,
                    child: GestureDetector(
                      onTap: () async {
                        final GpsCoordinates? result = await showDialog(
                            context: context, builder: (_) => AddPoint());
                          setState(() {
                            if (result != null) {
                              registeredCoordinates.add(result);
                            }
                          });
                      },
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context)!.steppercreatepoint,
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )*/
          ],
        ),
        isActive:_currentStep >= 0,
        //state: StepState.complete
      ),
      Step(
          state: StepState.indexed,
          title: Text(AppLocalizations.of(context)!.stepperend),
          content: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  AppLocalizations.of(context)!.stepperimg,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0,),
                ),
              ),
              SizedBox(height: 12.0),
                Row(
                  children: <Widget>[
                    SizedBox(height: 12.0,),
                    IconButton(
                        icon: Icon(Icons.camera_alt,
                          color: Colors.blue,),
                        onPressed: () {
                          getImage();
                        }
                    ),

                  ],
                ),
              SingleChildScrollView(
                child: _image == null ? Container() : Column(
                  children: <Widget>[
                    Container(
                      child: Image(image: FileImage(File(_image!.path)), height: 150, width: 150,),//Image.file(_image!, height: 150.0, width: 250.0,),
                    ),
                    SizedBox(height: 12.0),
                    TextFormField(
                      //focusNode:_focusNode,
                      keyboardType: TextInputType.text,
                      autocorrect: false,
                      onChanged: (String value){
                        setState(() {
                          data.fileNameController = value;
                        });
                      },
                      maxLines: 1,
                      validator: (value) {
                        if (value!.isEmpty || value.length < 1){
                          return AppLocalizations.of(context)!.stepperphoneerror7;
                        }
                      },
                      decoration: InputDecoration(
                        border: new OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.teal)
                        ),
                        labelText: AppLocalizations.of(context)!.fileName,
                        filled: true,
                        isDense: true,
                      ),
                    ),
                  ],
                )/*Image.network(_image.path, height: 50.0, width: 50.0,)*/,
              ),
            ],
          ),
        isActive:_currentStep >= 0,
      ),
    ];
    return _steps;
  }
}

class MyData {
  late String siteReferenceController;
  late String siteNameController;
  late String ownerNameController;
  late String ownerPhoneController;
  late String fileNameController;
  late District districtController;
  late String ownerCNIController;

}