import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get_it/get_it.dart';
import 'package:gos/components/form_error.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/constants.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/models/agent.dart';
import 'package:gos/pages/add_site.dart';
import 'package:gos/pages/change_password.dart';
import 'package:gos/pages/remember_agent_password.dart';
import 'package:gos/service/http_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void  setupLocator() {
  GetIt.I.registerLazySingleton((() => HTTPService()));
}

void main() {
  setupLocator();
  runApp(MyHomePage());
}

class MyHomePage extends StatefulWidget {
  MyHomePage();

  //final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final List<String> errors = [];
  late bool checkBoxValue;
  late bool _autovalidate;
  bool _isObscure = true;
  late TextEditingController _userNameController;
  late TextEditingController _passwordController;

  late APIResponse<dynamic> _apiResponse;
  void addError({required String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  final http = HTTPService();

  void removeError({required String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }
  String errorMessage = "";

  @override
  void initState(){
    EasyLoading.dismiss();
    super.initState();
    errorMessage = "";
    checkBoxValue = false;
    _autovalidate = false;
    _userNameController = TextEditingController();
    _passwordController = TextEditingController();
  }

  clearTextField(){
    _userNameController.clear();
    _passwordController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('PAGEGOS'),
        actions: [
          LanguagePickerWidget(),
          const SizedBox(width: 12),
        ],
        //automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(50.0),
        child: loginForm(),
      ),
    );
  }

  Widget loginForm(){
    return Form(
      key: _key,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Text(
              AppLocalizations.of(context)!.connexion,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 12.0),
          Text(
            errorMessage,
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
          ),
          SizedBox(height: 12.0),
          SizedBox(height: 12.0),
          buildLoginFormField(),
          SizedBox(height: 12.0),
          buildPwdFormField(),
          //SizedBox(height: 12.0),//remove after
          //buildIpFormField(),//remove after
          FormError(errors: errors),
           Container(
            padding: EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                new Checkbox(value: checkBoxValue,
                    activeColor: Colors.blueAccent,
                    onChanged:(bool? newValue){
                      setState(() {
                        checkBoxValue = newValue!;
                      });
                    }),
                Text(AppLocalizations.of(context)!.remember,),
              ],
            ),
          ),

          RaisedButton(
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
              padding: const EdgeInsets.all(16),
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),
              child: Text(AppLocalizations.of(context)!.connexionbtn,),
              onPressed:()  async {
                if (_key.currentState!.validate()) {
                  EasyLoading.show(status: (AppLocalizations.of(context)!.wait));
                  APIResponse<dynamic> response, sectorAgent;
                  Agent newAgent = new Agent(agentDeviceList: [],agentId: 0,agentName: "",agentPhoneNumber: "",agentSectorList: [],isFirstConnection: true,status: 0,agentLogin: _userNameController.text, agentPassword: _passwordController.text);
                  setState(() {
                    _passwordController.text = "";
                    _userNameController.text = "";
                  });
                 // response = await locator.get<HTTPService>().connectAgentWithLogin(newAgent);
                  response = await http.connectAgentWithLogin(newAgent);
                  setState(() {
                    _apiResponse = response;
                  });
                  if (_apiResponse.error == false) {
                    Util.connectedAgent = _apiResponse.data;
                    //sectorAgent = await locator.get<HTTPService>().getAgentSectors(Util.connectedAgent!);
                    sectorAgent = await http.getAgentSectors(Util.connectedAgent!);
                    Util.sectorAgent = sectorAgent.data;
                    if(Util.sectorAgent == null){
                      EasyLoading.showError(AppLocalizations.of(context)!.erroraffect, duration: duree);
                    }else{
                      EasyLoading.showSuccess(Util.connectedAgent!.agentName!,duration: duree);
                      if(Util.connectedAgent!.isFirstConnection! == true){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ChangePassword()),
                        );
                      }else{
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AddSitePage(
                            agent: newAgent,
                          )),
                        );
                      }
                    }

                  }
                  else{
                    EasyLoading.dismiss();
                    if(_apiResponse.errorMessage == "Unable to contact the server"){
                      setState(() {
                        errorMessage = AppLocalizations.of(context)!.errorserver;
                      });
                    }else{
                      setState(() {
                        errorMessage = AppLocalizations.of(context)!.erroruserlogin;
                      });
                    }
                    EasyLoading.showError(errorMessage, duration: duree);

                  }
                }
              }
          ),

          /*Container(
            padding: EdgeInsets.all(10.0),
            alignment: Alignment.center,
            child: InkWell(
              child: Text(AppLocalizations.of(context)!.forget,
                  style: TextStyle(
                    color: Colors.blueAccent,
                    fontFamily: 'Montserrat',
                  )
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RememberPasswordPage()),
                );
              },
            ),
          ),*/
        ],
      ),
    );
  }

  TextFormField buildPwdFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      obscureText: _isObscure,
      onSaved: (newValue) {
        setState(() {
          _passwordController.text = newValue!;
        });
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: AppLocalizations.of(context)!.errorpwd);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: AppLocalizations.of(context)!.errorpwd);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        border: new OutlineInputBorder(
            borderSide: new BorderSide(color: Colors.teal)
        ),
        labelText: AppLocalizations.of(context)!.pwd,
        suffixIcon: IconButton(
          icon: Icon(_isObscure ? Icons.visibility : Icons.visibility_off),
          onPressed: (){
            setState(() {
              _isObscure = !_isObscure;
            });
          },
        ),
       // hintText: "Nom d'utilisateur",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        filled: true,
        isDense: true,

      ),
      controller: _passwordController,
    );
  }

  TextFormField buildLoginFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) {
        setState(() {
          _userNameController.text = newValue!;
        });
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: AppLocalizations.of(context)!.errorusername);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: AppLocalizations.of(context)!.errorusername);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        border: new OutlineInputBorder(
            borderSide: new BorderSide(color: Colors.teal)
        ),
        labelText: AppLocalizations.of(context)!.login,
        suffixIcon: Icon(Icons.account_circle_sharp),
        //hintText: "Nom d'utilisateur",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        filled: true,
        isDense: true,
      ),
      controller: _userNameController,
    );
  }


}
