import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/models/district.dart';
import 'package:gos/helper/util.dart';

import 'package:gos/constants.dart';


import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:gos/models/site.dart';
import 'package:gos/pages/list_of_agent_site.dart';
import 'package:gos/service/http_service.dart';

class DistrictDetail extends StatefulWidget {
  final District district;
  DistrictDetail({Key? key, required this.district}) : super(key: key);

  @override
  _DistrictDetailState createState() => _DistrictDetailState();
}

class _DistrictDetailState extends State<DistrictDetail> {
  String errorMessage = "";
  late District _district;

  @override
  void initState() {

    super.initState();
    _district = widget.district;
    EasyLoading.dismiss();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text("District DETAIL"),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
          const SizedBox(width: 12),
        ],
      ),
    body: SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Column(
          children: <Widget>[
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    //leading: Icon(Icons.arrow_drop_down_circle),
                    title: Center(
                      child: Text(
                        _district.districtName.toUpperCase(),
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0,
                        color: Colors.blue),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: Column(
                      children: [
                        /*Image(image: AssetImage('assets/images/ynde.png')),*/
                        ListTile(
                          //leading: Icon(Icons.arrow_drop_down_circle),
                          title: Text(AppLocalizations.of(context)!.commune+": "+_district.commune),
                        ),
                        ListTile(
                          //leading: Icon(Icons.arrow_drop_down_circle),
                          title: Text(AppLocalizations.of(context)!.sector+": "+Util.sectorAgent!.sectorName),
                        ),
                        ListTile(
                          //leading: Icon(Icons.arrow_drop_down_circle),
                          title: Text("Standing: "+_district.standing),
                        ),
                        ListTile(
                          //leading: Icon(Icons.arrow_drop_down_circle),
                          title: Text(AppLocalizations.of(context)!.density+": "+_district.densite),
                        ),
                        ListTile(
                          //leading: Icon(Icons.arrow_drop_down_circle),
                          title: Text("Population: "+_district.population),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Image(image: AssetImage('assets/images/ynde.png')),
          ],
        ),
      ),
    ),
    floatingActionButton: FloatingActionButton.extended(
        foregroundColor: Colors.black,
        onPressed: () async{
          EasyLoading.show(status: AppLocalizations.of(context)!.wait);
          APIResponse<dynamic> response;
          response = await locator.get<HTTPService>().getSites(_district);
          if(response.error == false){
            List<Site> sites = response.data;
            Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => ListOfAgentSitePage(sites: sites,
                  )
              ),
            );

          }else{
            EasyLoading.showInfo(AppLocalizations.of(context)!.vide, duration: duree);
          }
          // Respond to button press
        },
        icon: Icon(Icons.visibility),
        label: Text('Sites'),
      ),
    );
  }
}
