import 'package:flutter/material.dart';
import 'package:gos/pages/login.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    // demo time it required to load inital data
    Future.delayed(
      Duration(seconds: 03),
      () => Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => MyHomePage(/*title: 'gos',*/),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Spacer(flex: 6),
            Center(
              child: Image(image: AssetImage('assets/icons/logocuy.png')),
            ),
            Center(
              child: Text('PAGEGOS'),
            ),
            Spacer(),
            Center(
              child: CircularProgressIndicator(),
            ),
            Spacer(),
            Text(
              AppLocalizations.of(context)!.wait,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Theme.of(context)
                    .textTheme
                    .bodyText1
                    !.color
                    !.withOpacity(0.64),
              ),
            ),
            Spacer(flex: 3),
          ],
        ),
      ),
    );
  }
}
