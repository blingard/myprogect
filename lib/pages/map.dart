import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/models/gps_coordinates.dart';

class MapMobile extends StatefulWidget {
  final List<GpsCoordinates> coordinates;
  final String siteName;
  const MapMobile({required this.coordinates, required this.siteName,});

  @override
  _MapMobileState createState() => _MapMobileState();
}

class _MapMobileState extends State<MapMobile> {
  late List<Polygon> _polygon;
  List<GpsCoordinates> coordinate=[];
  String _siteName = "";


  @override
  void initState() {
    super.initState();
    EasyLoading.dismiss();
    _siteName = widget.siteName;

    GpsCoordinates lat;
    for(GpsCoordinates g in widget.coordinates){
      lat = new GpsCoordinates(gpsCoordId: g.gpsCoordId, pointName:g.pointName, siteId: g.siteId, xcoord: g.ycoord, ycoord: g.xcoord );
      coordinate.add(lat);
    }

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(_siteName),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
          const SizedBox(width: 12),
        ],

      ),
      body: FlutterMap(
        options: MapOptions(
          zoom: 18.0,
          maxZoom: 22.0,
          minZoom: 1.0,
          center: LatLng(coordinate[0].xcoord,coordinate[0].ycoord ),
        ),
        layers: [
          PolygonLayerOptions(

          ),
          TileLayerOptions(
              urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
              subdomains: ['a', 'b', 'c']
          ),
          MarkerLayerOptions(
            markers: [
              Marker(
                width: 80.0,
                height: 80.0,
                point: LatLng(coordinate[0].xcoord,coordinate[0].ycoord),
                builder: (ctx) =>
                    Container(
                      child: Icon(Icons.add_location, color: Colors.red,),
                    ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
