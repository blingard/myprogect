import 'package:flutter/material.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/constants.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/models/agent.dart';
import 'package:gos/service/http_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RememberPasswordPage extends StatefulWidget{
  @override
  _RememberPasswordPageState createState() =>  _RememberPasswordPageState();

}

class _RememberPasswordPageState extends State<RememberPasswordPage>{

  final TextEditingController _userNameController = TextEditingController();
  final TextEditingController _mailAddressController = TextEditingController();

  late APIResponse<dynamic> _apiResponse;

  String errorMessage = "";

  clearTextField(){
    _userNameController.clear();
    _mailAddressController.clear();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PAGEGOS'),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
          const SizedBox(width: 12),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
        //color: Colors.black.withOpacity(0.1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            SizedBox(height: 40.0),

            Container(
              alignment: Alignment.center,
              child: Text(AppLocalizations.of(context)!.resertpwd,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
            ),
            SizedBox(height: 40.0),

            TextFormField(
              controller: _userNameController,
              decoration: InputDecoration(labelText: 'login'),
            ),
            SizedBox(height: 15.0),

            TextField(
              controller: _mailAddressController,
              decoration: InputDecoration(labelText:'email'),
            ),
            SizedBox(height: 40.0),

            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(width: 50.0),
                Container(
                  height: 50.0,
                  width: 100.0,
                  child: Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: Colors.transparent,
                    color: Colors.green,
                    elevation: 10.0,
                    child: GestureDetector(
                      onTap: () async {
                        APIResponse<dynamic> response;
                        Agent agent = new Agent(agentLogin: _userNameController.text, agentPhoneNumber: _mailAddressController.text,isFirstConnection: null,agentDeviceList: null, agentSectorList: null,status: null,agentName: null,agentId: null, agentPassword: null );
                        response = await locator.get<HTTPService>().rememberAgentPassword(agent);

                        setState(() {
                          _apiResponse = response;
                        });

                        if(_apiResponse.error == false){
                          clearTextField();
                        }else{
                          setState(() {
                            errorMessage = _apiResponse.errorMessage!;
                          });
                        }
                      },
                      child: Center(
                        child: Text(
                          'adddocsave',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 20.0),

                Container(
                  height: 50.0,
                  width: 100.0,
                  child: Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: Colors.transparent,
                    color: Colors.redAccent.withOpacity(0.8),
                    elevation: 10.0,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Center(
                        child: Text(
                          'adddoccancel',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

}