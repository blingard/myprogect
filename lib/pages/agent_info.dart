
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/pages/change_password.dart';
import 'package:gos/pages/home_drawer.dart';
import 'package:gos/pages/modify_agent_info.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AgentInfo extends StatefulWidget{
  //final List<Notifications> notifications;

  AgentInfo();
  @override
  _AgentInfoState createState() => _AgentInfoState();
}

class _AgentInfoState extends State<AgentInfo>{
  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
    EasyLoading.dismiss();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PAGEGOS'),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
        ],

      ),
      drawer: HomeDrawer(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Text(AppLocalizations.of(context)!.agentinfos,
                style: TextStyle(color: Colors.blue, fontSize: 18),),
              ),
              SizedBox(height: 12.0),
              Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ListTile(
                      title: Text(AppLocalizations.of(context)!.agentnumber,
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                      ),
                      subtitle: Text(Util.connectedAgent!.agentPhoneNumber!,
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                      ),
                    ),
                    ListTile(
                      title: Text(AppLocalizations.of(context)!.agentname,
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      subtitle: Text(Util.connectedAgent!.agentName!,
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                    ListTile(
                      title: Text(AppLocalizations.of(context)!.login,
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      subtitle: Text(Util.connectedAgent!.agentLogin,
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    )
                  ],
                ) ,
              ),
              SizedBox(height: 15.0),
              RaisedButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  padding: const EdgeInsets.all(20),
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),
                  child: Text(AppLocalizations.of(context)!.changepwdbtn),
                  onPressed:() {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ChangePassword()),
                    );
                  }
              ),
              SizedBox(height: 15.0),
              RaisedButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  padding: const EdgeInsets.all(20),
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),
                  child: Text(AppLocalizations.of(context)!.adddocsave),
                  onPressed:() {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ModifyAgentInfo()),
                    );
                  }
              ),
            ],
          ),
        ),
      ),
    );
  }

}