import 'dart:io';


import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/constants.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/models/agent.dart';
import 'package:gos/models/doctype.dart';
import 'package:gos/models/documents.dart';
import 'package:gos/models/site.dart';
import 'package:gos/pages/site_detail.dart';
import 'package:gos/service/http_service.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddDocument extends StatefulWidget{
  final Site site;
  AddDocument({required this.site});

  @override
  AddDocumentState createState() => new AddDocumentState();

}

class AddDocumentState extends State<AddDocument>{


  String errorMessage = "";
  Agent? agent = Util.connectedAgent;
  List<String> liste = ['Suivi','Convocation','Titre Foncier',"Certificat d'Urbanisme",'Certificat de Conformité','Permit de Construire',"Permit d'Implanter",'Servitude Publique','Modification du Lotissement', "Empiétement du domaine public de l'Etat", "Empiétement du domaine prive de l'Etat", "Empiétement du domaine des Collectivites Territoriales Decentralisées", "Non Respect des règles générales d'Urbanisme et de Construction"];
  String? acte;
  String? numeroConvocation;

  APIResponse<String>? _apiResponseUploadedPath;
  APIResponse<dynamic>? _apiResponse;
  bool _isButtonEnabled = true;
  APIResponse<List<DocType>>? _apiResponseDoc;
  void _incrementCounter() async {
    if(_isButtonEnabled == false){

    }else{
      EasyLoading.show(status: AppLocalizations.of(context)!.wait);
      if(nameDoc == null){
        EasyLoading.showInfo(AppLocalizations.of(context)!.namedoc, duration: duree);
      }else{
        if(acte == null){
          EasyLoading.showInfo(AppLocalizations.of(context)!.chose, duration: duree);
        }else if(acte == "Convocation" || acte == "Titre Foncier" || acte == "Certificat d'Urbanisme" || acte == "Certificat de Conformité" || acte == "Permit de Construire" || acte == "Permit d'Implanter"){
          if(numeroConvocation==null || numeroConvocation!.length<3){
            EasyLoading.showInfo(AppLocalizations.of(context)!.convoc, duration: duree);
          }else{
            setState(() {
              _isButtonEnabled = false;
            });
            APIResponse<dynamic> response;
            APIResponse<
                dynamic> responseUploadMessage = await locator
                .get<HTTPService>().uploadImage(_image!);
            setState(() {
              _apiResponseUploadedPath =
              responseUploadMessage as APIResponse<String>;
            });
            if (responseUploadMessage.error == false) {
              responseUploadMessage = await locator.get<HTTPService>().getDocTypes();
              List <DocType> docTypes = responseUploadMessage.data;
              int i = docTypes.indexWhere((element) => element.docTypeName=="IMAGE");
              DocType docType= docTypes[i];
              String codeActe = "";
              int status = 0;
              if(acte == "Convocation"){
                status = 3;
                codeActe = "CO";
              }else if(acte == "Titre Foncier"){
                status = 5;
                codeActe = "TF";
              }else if(acte == "Certificat d'Urbanisme"){
                status = 5;
                codeActe = "CU";
              }else if(acte == "Certificat de Conformité"){
                status = 5;
                codeActe = "CC";
              }else if(acte == "Permit de Construire"){
                status = 5;
                codeActe = "PC";
              }else if(acte == "Permit d'Implanter"){
                status = 5;
                codeActe = "PI";
              }
              Documents doc = new Documents(
                  docTypeId: docType,
                  siteId: widget.site,
                  documentNumber: numeroConvocation!,
                  documentCode: codeActe,
                  documentName: acte!,
                  documentPath: _apiResponseUploadedPath!
                      .data,
                  status: status,
                  createdDate: null,
                  documentExtension: null,
                  documentId: null,
                  secretKey: null);
              response = await locator.get<HTTPService>().createDocument(
                  doc, Util.connectedAgent!.agentId!, acte!);
              if (response.error == false) {
                EasyLoading.showSuccess('', duration: duree);
                _image=null;
                setState(() {
                  _isButtonEnabled = true;
                });
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      SiteDetailPage(
                          site: widget.site
                      )
                  ),
                );
              } else {
                EasyLoading.showError(response.errorMessage!, duration: duree);
                setState(() {
                  _isButtonEnabled = true;
                });

              }
            }
          }
        }else{
          APIResponse<dynamic> response;
          APIResponse<
              dynamic> responseUploadMessage = await locator
              .get<HTTPService>().uploadImage(_image!);
          setState(() {
            _apiResponseUploadedPath =
            responseUploadMessage as APIResponse<String>;
          });
          if (responseUploadMessage.error == false) {
            responseUploadMessage = await locator.get<HTTPService>().getDocTypes();
            List <DocType> docTypes = responseUploadMessage.data;
            int i = docTypes.indexWhere((element) => element.docTypeName=="IMAGE");
            DocType docType= docTypes[i];
            Documents doc = new Documents(
                docTypeId: docType,
                siteId: widget.site,
                documentName: nameDoc!,
                documentCode: acte!,
                documentNumber: null,
                documentPath: _apiResponseUploadedPath!
                    .data,
                status: 0,
                createdDate: null,
                documentExtension: null,
                documentId: null,
                secretKey: null);
            response = await locator.get<HTTPService>().createDocument(
                doc, Util.connectedAgent!.agentId!, acte!);
            if (response.error == false) {
              EasyLoading.showSuccess('', duration: duree);
              _image=null;
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>
                    SiteDetailPage(
                        site: widget.site
                    )
                ),
              );
            } else {
              EasyLoading.showError(response.errorMessage!, duration: duree);
            }
          }
        }

      }
    }


  }

  //AudioPlayer audioPlayer;
  // AudioCache audioCache = AudioCache();

  List<String>? docPaths;
  String? nameDoc ;
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
  }

  File? _image = null;
  final picker = ImagePicker();

  Future getImage() async{
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }




  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        appBar: AppBar(
          bottom: const TabBar(
            tabs: [
              Tab(icon: Icon(Icons.image)),
            ],
          ),
          title: Text(AppLocalizations.of(context)!.adddocadd),
          centerTitle: true,
          actions: [
            LanguagePickerWidget(),
            const SizedBox(width: 12),
          ],
        ),
        body: TabBarView(
          children: [
            SingleChildScrollView(
                child: Center(
                  child: _image ==null ?
                  Container()               :
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 10.0),
                        TextFormField(
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onChanged: (String value){
                            setState(() {
                              nameDoc = value;
                            });
                          },
                          maxLines: 1,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 1){
                              return AppLocalizations.of(context)!.stepperphoneerror7;
                            }
                          },
                          decoration: InputDecoration(
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.teal)
                            ),
                            labelText: AppLocalizations.of(context)!.namedoc,
                            filled: true,
                            isDense: true,
                          ),
                        ),
                        SizedBox(height: 10.0),
                        DropdownSearch<String>(
                          mode: Mode.BOTTOM_SHEET,
                          //showSelectedItem: true,
                          items: liste,
                          label: AppLocalizations.of(context)!.chose,
                          //popupItemDisabled: (String s) => s.startsWith(''),
                          onChanged: (a){
                            setState(() {
                              acte=a!;
                            });
                          },
                          validator: (value){
                            if (value!.isEmpty || value.length < 1){
                              return AppLocalizations.of(context)!.stepperphoneerror7;
                            }
                          },
                        ),
                        SizedBox(height: 12.0),
                        acte == 'Convocation' ?
                        TextFormField(
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onChanged: (String value){
                            setState(() {
                              numeroConvocation = value;
                            });
                          },
                          maxLines: 1,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 1){
                              return AppLocalizations.of(context)!.stepperphoneerror7;
                            }
                          },
                          decoration: InputDecoration(
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.teal)
                            ),
                            labelText: "N°",
                            filled: true,
                            isDense: true,
                          ),
                        ):
                        SizedBox(height: 12.0),
                        acte == 'Titre Foncier' ?
                        TextFormField(
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onChanged: (String value){
                            setState(() {
                              numeroConvocation = value;
                            });
                          },
                          maxLines: 1,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 1){
                              return AppLocalizations.of(context)!.stepperphoneerror7;
                            }
                          },
                          decoration: InputDecoration(
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.teal)
                            ),
                            labelText: "N°",
                            filled: true,
                            isDense: true,
                          ),
                        ):
                        SizedBox(height: 12.0),
                        acte == "Certificat d'Urbanisme" ?
                        TextFormField(
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onChanged: (String value){
                            setState(() {
                              numeroConvocation = value;
                            });
                          },
                          maxLines: 1,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 1){
                              return AppLocalizations.of(context)!.stepperphoneerror7;
                            }
                          },
                          decoration: InputDecoration(
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.teal)
                            ),
                            labelText: "N°",
                            filled: true,
                            isDense: true,
                          ),
                        ):
                        SizedBox(height: 12.0),
                        acte == 'Certificat de Conformité'  ?
                        TextFormField(
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onChanged: (String value){
                            setState(() {
                              numeroConvocation = value;
                            });
                          },
                          maxLines: 1,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 1){
                              return AppLocalizations.of(context)!.stepperphoneerror7;
                            }
                          },
                          decoration: InputDecoration(
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.teal)
                            ),
                            labelText: "N°",
                            filled: true,
                            isDense: true,
                          ),
                        ):
                        SizedBox(height: 12.0),
                        acte == 'Permit de Construire' ?
                        TextFormField(
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onChanged: (String value){
                            setState(() {
                              numeroConvocation = value;
                            });
                          },
                          maxLines: 1,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 1){
                              return AppLocalizations.of(context)!.stepperphoneerror7;
                            }
                          },
                          decoration: InputDecoration(
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.teal)
                            ),
                            labelText: "N°",
                            filled: true,
                            isDense: true,
                          ),
                        ):
                        SizedBox(height: 12.0),
                        acte == "Permit d'Implanter" ?
                        TextFormField(
                          keyboardType: TextInputType.text,
                          autocorrect: false,
                          onChanged: (String value){
                            setState(() {
                              numeroConvocation = value;
                            });
                          },
                          maxLines: 1,
                          validator: (value) {
                            if (value!.isEmpty || value.length < 1){
                              return AppLocalizations.of(context)!.stepperphoneerror7;
                            }
                          },
                          decoration: InputDecoration(
                            border: new OutlineInputBorder(
                                borderSide: new BorderSide(color: Colors.teal)
                            ),
                            labelText: "N°",
                            filled: true,
                            isDense: true,
                          ),
                        ):
                        SizedBox(height: 12.0),
                        Container(
                          child: Image(image: FileImage(File(_image!.path)),),//Image.file(_image!, height: 150.0, width: 250.0,),
                        ),


                      ],
                    ),
                  ),)
            ),
          ],
        ),
        floatingActionButton: _image ==null ?
        FloatingActionButton.extended(
          foregroundColor: Colors.black,
          onPressed: () {
            getImage();
          },
          icon: Icon(Icons.image),
          label: Text('Image'),
        ):
        FloatingActionButton.extended(
          foregroundColor: Colors.black,
          onPressed: _isButtonEnabled ? _incrementCounter : null,
          icon: Icon(Icons.save),
          label: Text(AppLocalizations.of(context)!.adddocsave),
        ),
      ),
    );
    /*DefaultTabController(
      initialIndex: 1,
      length: 1,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('TabBar Widget'),
          bottom: const TabBar(
            tabs: <Widget>[
              Tab(
                //icon: Icon(Icons.image_outlined),
                text: "IMAGE",
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            Container(
              child: Center(
                child: IconButton(
                    icon: Icon(Icons.file_copy, color: Colors.blue,),
                    iconSize: 30.0,
                    onPressed: () {

                    }
                ),
              )
                  :
              SingleChildScrollView(
                child: Column(
                  children: [
                    Image.file(_image!),
                    TextButton(
                      child: Text(AppLocalizations.of(context)!.adddocsave),
                      onPressed: () async{
                        APIResponse<dynamic> response;
                        APIResponse<dynamic> responseUploadMessage =  await locator.get<HTTPService>().uploadImage(_image!);
                        setState(() {
                          _apiResponseUploadedPath = responseUploadMessage as APIResponse<String>;
                        });
                        if (_apiResponseUploadedPath!.error == false) {
                          print('site ID ${widget.site.siteId}');
                          DocType docType = new DocType(docTypeId: docTypeId,documentsList: null,docTypeName: null,description: null);
                          Documents doc = new Documents(docTypeId: docType, siteId: widget.site, documentName: _apiResponseUploadedPath!.data ,documentPath: _apiResponseUploadedPath!.data,status: null,createdDate: null,documentExtension: null,documentId: null,secretKey: null);
                          widget.docList!.data.add(doc);

                          response = await locator.get<HTTPService>().createDocument(doc, Util.connectedAgent!.agentId!);

                          setState(() {
                            _apiResponse = response;
                            //responseSite = _apiResponseSite;
                          });

                          Navigator.of(context).pop();

                          print('Document List ${widget.docList!.data}');

                          if(_apiResponse!.error == false){
                            print('Document added successfully');
                          }else{
                            setState(() {
                              errorMessage = _apiResponse!.errorMessage;
                            });
                          }

                        }
                      },
                    ),
                    TextButton(
                      child: Text(AppLocalizations.of(context)!.adddoccancel),
                      onPressed: () {
                        setState(() {
                          _image = null;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );*/

  }

}