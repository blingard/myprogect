
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/constants.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/pages/agent_info.dart';
import 'package:gos/pages/home_drawer.dart';
import 'package:gos/service/http_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChangePassword extends StatefulWidget{
  ChangePassword();
  @override
  _ChangePasswordState createState() => _ChangePasswordState();

}

class _ChangePasswordState extends State<ChangePassword>{

  TextEditingController oldPassword = TextEditingController();
  TextEditingController newPassword = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  bool _isObscureoldPassword = true;
  bool _isObscurenewPassword = true;
  bool _isObscureconfirmPassword = true;

  late APIResponse<dynamic> _apiResponse;

  String errorMessage = "";
  @override
  void initState()  {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        title: Text('PAGEGOS'),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
        ],

      ),
      drawer: HomeDrawer(),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: Text(
                  AppLocalizations.of(context)!.changepwd,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 15.0),

              TextFormField(
                controller: oldPassword,
                keyboardType: TextInputType.text,
                obscureText: _isObscureoldPassword,
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),
                  labelText: AppLocalizations.of(context)!.oldpwd,
                  suffixIcon: IconButton(
                    icon: Icon(_isObscureoldPassword ? Icons.visibility : Icons.visibility_off),
                    onPressed: (){
                      setState(() {
                        _isObscureoldPassword = !_isObscureoldPassword;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(height: 15.0),
              TextFormField(
                controller: newPassword,
                keyboardType: TextInputType.text,
                obscureText: _isObscurenewPassword,
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),
                  labelText: AppLocalizations.of(context)!.newpwd,
                  suffixIcon: IconButton(
                    icon: Icon(_isObscurenewPassword ? Icons.visibility : Icons.visibility_off),
                    onPressed: (){
                      setState(() {
                        _isObscurenewPassword = !_isObscurenewPassword;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(height: 15.0),
              TextFormField(
                controller: confirmPassword,
                keyboardType: TextInputType.text,
                obscureText: _isObscureconfirmPassword,
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),
                  labelText: AppLocalizations.of(context)!.newpwd,
                  suffixIcon: IconButton(
                    icon: Icon(_isObscureconfirmPassword ? Icons.visibility : Icons.visibility_off),
                    onPressed: (){
                      setState(() {
                        _isObscureconfirmPassword = !_isObscureconfirmPassword;
                      });
                    },
                  ),
                ),
              ),
              SizedBox(height: 15.0),
              Text(
                errorMessage,
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
              ),
              SizedBox(height: 15.0),
              RaisedButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  padding: const EdgeInsets.all(20),
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),
                  child: Text(AppLocalizations.of(context)!.adddocsave),
                  onPressed:() async{
                    EasyLoading.show(status: AppLocalizations.of(context)!.wait);
                    APIResponse<dynamic> response;
                    if(newPassword.text == confirmPassword.text){
                      response = await locator.get<HTTPService>().changeAgentPassword(Util.connectedAgent!, oldPassword.text, confirmPassword.text);
                      setState(() {
                        _apiResponse = response;
                      });
                    }
                    if(_apiResponse.error == false){
                      EasyLoading.showSuccess("", duration: duree);
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AgentInfo()),
                      );
                    }else{
                      EasyLoading.showError(AppLocalizations.of(context)!.pwderr, duration: duree);
                      setState(() {
                        errorMessage = AppLocalizations.of(context)!.pwderr;
                      });
                    }
                  }
              ),
            ],
          ),
        ),
      ),
    );
  }

}