import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/models/documents.dart';
import 'package:gos/service/http_service.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LastDocument extends StatelessWidget {
  final Documents document;
  const LastDocument({Key? key, required this.document}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    EasyLoading.dismiss();

    return Scaffold(
      appBar: AppBar(
        title: Text(document.documentName!),
        centerTitle: true,
        actions: [
          LanguagePickerWidget(),
          const SizedBox(width: 12),
        ],
      ),
      body: Stack(
          children: <Widget>[
            Center(child: CircularProgressIndicator(semanticsLabel: AppLocalizations.of(context)!.wait)),
            Center(
              child: FadeInImage.memoryNetwork(
                placeholder: kTransparentImage,
                image: HTTPService.FileAPI+"download?fileKey="+document.documentPath!,
              ),
            ),
          ],
        ),
    );
  }
}
