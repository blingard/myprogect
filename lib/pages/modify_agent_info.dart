import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:gos/components/language_picker_widget.dart';
import 'package:gos/constants.dart';
import 'package:gos/helper/api_response.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/models/agent.dart';
import 'package:gos/pages/agent_info.dart';
import 'package:gos/pages/home_drawer.dart';
import 'package:gos/service/http_service.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ModifyAgentInfo extends StatefulWidget{
  ModifyAgentInfo();
  @override
  _ModifyAgentInfoState createState() => _ModifyAgentInfoState();

}

class _ModifyAgentInfoState extends State<ModifyAgentInfo>{


  TextEditingController agentNameController = TextEditingController();
  TextEditingController agentPhoneController = TextEditingController();

  late APIResponse<dynamic> _apiResponse;

  String errorMessage = "";

  @override
  void initState()  {
    EasyLoading.dismiss();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
       //automaticallyImplyLeading: false,
       title: Text(AppLocalizations.of(context)!.profile),
       centerTitle: true,
       actions: [
         LanguagePickerWidget(),
       ],

     ),
     drawer: HomeDrawer(),
     body: SingleChildScrollView(
       padding: EdgeInsets.fromLTRB(10.0, 10.0, 20.0, 20.0),
       child: Column(
         children: <Widget>[
           Container(
             alignment: Alignment.topLeft,
             child: Text(AppLocalizations.of(context)!.stepperdetails+" / "+AppLocalizations.of(context)!.editprofile,
               style: TextStyle(color: Colors.blue, fontSize: 18),),
           ),
           SizedBox(height: 15.0),
           TextFormField(
             controller: agentNameController,
             decoration: InputDecoration(labelText: AppLocalizations.of(context)!.agentname+"*"),
           ),
           SizedBox(height: 15.0),
           TextFormField(
             controller: agentPhoneController,
             decoration: InputDecoration(labelText: AppLocalizations.of(context)!.agentnumber+"*"),
           ),
           SizedBox(height: 15.0),
           Row(
             children: <Widget>[
               RaisedButton(
                   color: Colors.green,
                   textColor: Colors.white,
                   padding: const EdgeInsets.all(20),
                   shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),
                   child: Text(AppLocalizations.of(context)!.adddocsave),
                   onPressed:() async{
                     EasyLoading.show( status: AppLocalizations.of(context)!.wait);
                     APIResponse<dynamic> response;
                     Agent agent = new Agent(agentPassword: Util.connectedAgent!.agentPassword, agentLogin: Util.connectedAgent!.agentLogin, agentId: Util.connectedAgent!.agentId ,agentName: agentNameController.text, agentPhoneNumber: agentPhoneController.text, status: Util.connectedAgent!.status,agentSectorList: Util.connectedAgent!.agentSectorList, agentDeviceList: Util.connectedAgent!.agentDeviceList, isFirstConnection: Util.connectedAgent!.isFirstConnection);
                     response = await locator.get<HTTPService>().updateAgentInfo(agent);

                     setState(() {
                       _apiResponse = response;
                     });
                     if(_apiResponse.error == false){
                       Util.connectedAgent = _apiResponse.data;
                       EasyLoading.showSuccess( "", duration: duree);
                       Navigator.push(
                         context,
                         MaterialPageRoute(builder: (context) => AgentInfo()),
                       );
                     }else{
                       EasyLoading.showError( _apiResponse.errorMessage!, duration: duree);
                       setState(() {
                         errorMessage = _apiResponse.errorMessage!;
                       });
                     }

                   }
               ),
               SizedBox(width: 10.0),
               RaisedButton(
                   color: Colors.red,
                   textColor: Colors.white,
                   padding: const EdgeInsets.all(20),
                   shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8.0)),
                   child: Text(AppLocalizations.of(context)!.adddoccancel),
                   onPressed:() async{
                     Navigator.push(
                       context,
                       MaterialPageRoute(builder: (context) => AgentInfo()),
                     );
                   }
               ),
             ],
           )
         ],
       ),
     ),
   );
  }

}