import 'package:flutter/material.dart';
import 'package:gos/models/site.dart';
import 'package:gos/pages/site_detail.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:gos/service/http_service.dart';

class NameSearch extends SearchDelegate<Site>{
  late final List<Site> names;
  NameSearch(this.names);
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: (){
            query = '';
          },
          icon: Icon(Icons.clear)
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {

    return IconButton(onPressed: () {
      close(context, this.names[0]);
    }, icon: Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    final suggestions = names.where((name) {
      return name.siteName.toLowerCase().contains(query.toLowerCase()) ;
    });
    return ListView.builder(
        itemCount: suggestions.length,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Center(
                child: Text(
                  suggestions.elementAt(index).siteName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20
                  ),
                )
            ),
            subtitle: Container(
              child: Column(
                children: <Widget>[
                  Text(AppLocalizations.of(context)!.siteowner +": "+ suggestions.elementAt(index).ownerName),
                  Text(AppLocalizations.of(context)!.sectorname +": "+ suggestions.elementAt(index).districtId.districtName),
                ],
              ),
            ),
            tileColor: suggestions.elementAt(index).status == 0 ? Colors.green : Colors.deepOrangeAccent,
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => SiteDetailPage(
                    site: suggestions.elementAt(index),
                  )
              ),
            ),
          );
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestions = names.where((name) {
      return name.siteName.toLowerCase().contains(query.toLowerCase()) ;
    });
    return ListView.builder(
        itemCount: suggestions.length,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Center(
                child: Text(
                  suggestions.elementAt(index).siteName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20
                  ),
                )
            ),
            subtitle: Container(
              child: Column(
                children: <Widget>[
                  Text(AppLocalizations.of(context)!.siteowner +": "+ suggestions.elementAt(index).ownerName),
                  Text(AppLocalizations.of(context)!.sectorname +": "+ suggestions.elementAt(index).districtId.districtName),
                ],
              ),
            ),
            tileColor: suggestions.elementAt(index).status == 0 ? Colors.green : Colors.deepOrangeAccent,
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => SiteDetailPage(
                    site: suggestions.elementAt(index),
                  )
              ),
            ),
          );
        });
  }
}