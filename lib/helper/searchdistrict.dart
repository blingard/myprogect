import 'package:flutter/material.dart';
import 'package:gos/helper/util.dart';
import 'package:gos/models/district.dart';
import 'package:gos/pages/district_detail.dart';

class DistrictSearch extends SearchDelegate<District>{
  late final List<District> names;
  DistrictSearch(this.names);
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: (){
            query = '';
          },
          icon: Icon(Icons.clear)
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(onPressed: () {
      close(context, this.names[0]);
    }, icon: Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    final suggestions = names.where((name) {
      return name.districtName.toLowerCase().contains(query.toLowerCase()) ;
    });
    return ListView.builder(
          itemCount: suggestions.length,
          itemBuilder: (BuildContext context, int index){
            return ListTile(
              title: Center(
                child: Text(
                  suggestions.elementAt(index).districtName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20
                  ),
                )
            ),
              subtitle: Container(
              child: Column(
                children: <Widget>[
                  Text("Commune: "+ suggestions.elementAt(index).commune),
                  Text("Secteur: "+ Util.sectorAgent!.sectorName),
                ],
              ),
            ),
            //tileColor: _apiResponse.elementAt(index).status == 0 ? Colors.green : Colors.deepOrangeAccent,
             onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => DistrictDetail(district: suggestions.elementAt(index))
              ),
            ),
          );
          }
        );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestions = names.where((name) {
      return name.districtName.toLowerCase().contains(query.toLowerCase()) ;
    });
    return ListView.builder(
        itemCount: suggestions.length,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Center(
                child: Text(
                  suggestions.elementAt(index).districtName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 20
                  ),
                )
            ),
            subtitle: Container(
              child: Column(
                children: <Widget>[
                  Text("Commune: "+ suggestions.elementAt(index).commune),
                  Text("Secteur: "+ Util.sectorAgent!.sectorName),
                ],
              ),
            ),
            //tileColor: _apiResponse.elementAt(index).status == 0 ? Colors.green : Colors.deepOrangeAccent,
            onTap: () => Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => DistrictDetail(district: suggestions.elementAt(index))
              ),
            ),
          );
        }
    );
  }
  
}