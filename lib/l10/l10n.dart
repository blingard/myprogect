import 'package:flutter/material.dart';

class L10n{
  static final all = [
    const Locale('en'),
    const Locale('fr'),
  ];

  static String getFlag(String code) {
    String? response;
    switch (code) {
      case 'fr':
        response = 'assets/images/fr.png';
        break;
      case 'en':
        response = 'assets/images/usa.jpg';
        break;
    }
    return response!;
  }
}