import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../size_config.dart';

class FormErr extends StatelessWidget {
  const FormErr({
    Key? key,
    required this.errors,
  }) : super(key: key);

  final String errors;

  @override
  Widget build(BuildContext context) {
    return formErrorText(error: errors);
  }

  Row formErrorText({String? error}) {
    return Row(
      children: [
        Icon(Icons.adjust_rounded, color: Colors.red,),
        Text(error!, style: TextStyle(color: Colors.red),),
      ],
    );
  }
}
