import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:gos/models/gps_coordinates.dart';
import 'package:gos/models/site.dart';
import 'package:provider/provider.dart';

class AddPoint extends StatefulWidget {
  @override
  _AddPointState createState() => _AddPointState();
}

class _AddPointState extends State<AddPoint> {
  Location location = new  Location();
  late bool _serviceEnable;
  late PermissionStatus _permissionGranted;
  late LocationData _locationData;
  bool _isGetLocation=false;









  final TextEditingController _locationNameController = TextEditingController();
  GpsCoordinates newPoint = GpsCoordinates(ycoord: 0.0, xcoord: 0.0);
  String createdPointLabel = '';
  @override
  Widget build(BuildContext context) {
    var localtionModel = Provider.of<GpsCoordinates>(context);
    return AlertDialog(
      title: Text('Ajouter un point'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(width: 10.0,),
          TextFormField(
            controller: _locationNameController,
            decoration: InputDecoration(labelText: 'Référence du point'),
          ),
          SizedBox(height: 10.0),
          SingleChildScrollView(
            child:  Row(
              children: <Widget>[
                SizedBox(width: 100.0,),
                Container(
                  alignment: Alignment.bottomLeft,
                  height: 40.0,
                  width: 70.0,
                  child:  Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: Colors.transparent,
                    color: Colors.blue,
                    elevation: 10.0,
                    child: GestureDetector(
                      onTap: () async{
                        _serviceEnable = await location.serviceEnabled();
                        if(!_serviceEnable){
                          _serviceEnable = await location.requestService();
                          if(_serviceEnable) return;
                        }

                        _permissionGranted = await location.hasPermission();
                        if(_permissionGranted == PermissionStatus.denied){
                          _permissionGranted = await location.requestPermission();
                          if(_permissionGranted != PermissionStatus.granted) return;
                        }
                        _locationData = await location.getLocation();
                        setState(() {
                          createdPointLabel = '0';
                          _isGetLocation = true;
                        });
                        setState(() {
                          newPoint.xcoord = _locationData.latitude!;
                          newPoint.ycoord = _locationData.latitude!;
                          createdPointLabel = 'Coordonnées '+ newPoint.xcoord.toString()+' ; '+newPoint.ycoord.toString();
                        });
                      },
                      child: Center(
                        child: Text(
                          'Localiser',
                          style: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 10.0,),
              ],
            ),
          ),
          Container(
            child: createdPointLabel=="0"
                ? CircularProgressIndicator()
                : Text(createdPointLabel),
          ),

        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Créer'),
          onPressed: () {
            setState(() {
              newPoint.pointName = _locationNameController.text;
            });
            Navigator.of(context).pop(newPoint);
          },
        ),
        FlatButton(
          child: Text('Annuler'),
          onPressed: () {
            Navigator.of(context).pop(null);
          },
        ),
      ],
    );
  }
}
