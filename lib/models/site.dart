import 'dart:core';
import 'package:gos/models/documents.dart';
import 'package:gos/models/gps_coordinates.dart';
import 'package:gos/models/sector.dart';
import 'package:json_annotation/json_annotation.dart';

import 'district.dart';

part 'site.g.dart';

@JsonSerializable()
class Site{
  Site({required this.siteId, required this.siteName, required this.refSite, required this.ownerName, required this.ownerPhone, required this.ownerCni,
    required this.status, required this.districtId, required this.documentsList, required this.gpsCoordinatesList});

  int? siteId;
  String siteName;
  String refSite;
  String ownerName;
  String ownerPhone;
  String ownerCni;
  int status;
  District districtId;
  List<Documents> documentsList;
  List<GpsCoordinates> gpsCoordinatesList;

  factory Site.fromJson(Map<String, dynamic> json) => _$SiteFromJson(json);
  Map<String, dynamic> toJson() => _$SiteToJson(this);

}