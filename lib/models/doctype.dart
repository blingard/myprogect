import 'dart:core';
import 'package:gos/models/documents.dart';
import 'package:json_annotation/json_annotation.dart';

part 'doctype.g.dart';

@JsonSerializable()
class DocType{
  DocType({required this.docTypeId, required this.docTypeName, required this.description, required this.documentsList});

  int? docTypeId;
  String? docTypeName;
  String? description;
  List<Documents>? documentsList;

  factory DocType.fromJson(Map<String, dynamic> json) => _$DocTypeFromJson(json);
  Map<String, dynamic> toJson() => _$DocTypeToJson(this);

}