// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Employee _$EmployeeFromJson(Map<String, dynamic> json) {
  return Employee(
    employeeId: json['employeeId'] as int,
    employeeName: json['employeeName'] as String,
    employeeSurname: json['employeeSurname'] as String,
    employeePassword: json['employeePassword'] as String,
    employeePhoneNumber: json['employeePhoneNumber'] as String,
    employeeMatricule: json['employeeMatricule'] as String,
    employeeLogin: json['employeeLogin'] as String,
    employeeBirthdate: json['employeeBirthdate'] as String,
    employeePlaceOfBirth: json['employeePlaceOfBirth'] as String,
    employeeNationality: json['employeeNationality'] as String,
    employeeAdress: json['employeeAdress'] as String,
    isFirstConnection: json['isFirstConnection'] as bool,
    employeeGender: json['employeeGender'] as String,
    status: json['status'] as int,
    employeeGroupEmployeeList: json['employeeGroupEmployeeList'] == null
        ? null
        : EmployeeAccessRights.fromJson(json['employeeGroupEmployeeList'] as Map<String, dynamic>),
    notificationList: json['notificationList'] as String,
  );
}

Map<String, dynamic> _$EmployeeToJson(Employee instance) =>
    <String, dynamic>{
      'employeeId': instance.employeeId,
      'employeeName': instance.employeeName,
      'employeeSurname': instance.employeeSurname,
      'employeePassword': instance.employeePassword,
      'employeePhoneNumber': instance.employeePhoneNumber,
      'employeeMatricule': instance.employeeMatricule,
      'employeeLogin': instance.employeeLogin,
      'employeeBirthdate': instance.employeeBirthdate,
      'employeePlaceOfBirth': instance.employeePlaceOfBirth,
      'employeeNationality': instance.employeeNationality,
      'employeeAdress': instance.employeeAdress,
      'isFirstConnection': instance.isFirstConnection,
      'employeeGender': instance.employeeGender,
      'status': instance.status,
      'employeeGroupEmployeeList': instance.employeeGroupEmployeeList,
      'notificationList': instance.notificationList,
    };
