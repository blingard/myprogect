import 'dart:core';
import 'package:json_annotation/json_annotation.dart';

part 'error_object.g.dart';

@JsonSerializable()
class ErrorObject{
  ErrorObject({this.errorCode, this.errorText});

  int? errorCode;
  String? errorText;

  factory ErrorObject.fromJson(Map<String, dynamic> json) => _$ErrorObjectFromJson(json);
  Map<String, dynamic> toJson() => _$ErrorObjectToJson(this);

}