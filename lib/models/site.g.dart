// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'site.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Site _$SiteFromJson(Map<String, dynamic> json) {
  return Site(
    siteId: json['siteId'] as int,
    siteName: json['siteName'] as String,
    refSite: json['refSite'] as String,
    ownerName: json['ownerName'] as String,
    ownerPhone: json['ownerPhone'] as String,
    ownerCni: json['ownerCni'] as String,
    status: json['status'] as int,
    districtId: json['districtId'] as District,
    documentsList: json['documentsList'] as List<Documents>,
    gpsCoordinatesList: json['gpsCoordinatesList'] as List<GpsCoordinates>,

  );
}


Map<String, dynamic> _$SiteToJson(Site instance) => <String, dynamic>{
      'siteId': instance.siteId,
      'siteName': instance.siteName,
      'refSite': instance.refSite,
      'ownerName': instance.ownerName,
      'ownerPhone': instance.ownerPhone,
      'ownerCni': instance.ownerCni,
      'status': instance.status,
      'districtId': instance.districtId,
      'documentsList': instance.documentsList,
      'gpsCoordinatesList': instance.gpsCoordinatesList,

    };
