// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'documents.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Documents _$DocumentsFromJson(Map<String, dynamic> json) {
  return Documents(
    documentId: json['documentId'] as int,
    documentName: json['documentName'] as String,
    documentPath: json['documentPath'] as String,
    secretKey: json['secretKey'] as String,
    documentExtension: json['documentExtension'] as String,
    documentCode: json['documentCode'] as String,
    documentNumber: json['documentNumber'] as String,
    createdDate: json['createdDate'] as String,
    status: json['status'] as int,
    docTypeId: json['docTypeId'] == null
        ? null
        : DocType.fromJson(json['docTypeId'] as Map<String, dynamic>),
    siteId: json['siteId'] == null
        ? null
        : Site.fromJson(json['siteId'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$DocumentsToJson(Documents instance) => <String, dynamic>{
  'documentId': instance.documentId,
  'documentName': instance.documentName,
  'documentPath': instance.documentPath,
  'documentCode': instance.documentCode,
  'documentNumber': instance.documentNumber,
  'secretKey': instance.secretKey,
  'documentExtension': instance.documentExtension,
  'createdDate': instance.createdDate,
  'status': instance.status,
  'docTypeId': instance.docTypeId,
  'siteId': instance.siteId,
};
