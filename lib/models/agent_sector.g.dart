// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agent_sector.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AgentSector _$AgentSectorFromJson(Map<String, dynamic> json) {
  return AgentSector(
    endDate: json['endDate'] as String,
    agentSectorPK: json['agentSectorPK'] == null
        ? null
        : AgentSectorPK.fromJson(json['agentSectorPK'] as Map<String, dynamic>),
    agent: json['agent'] == null
        ? null
        : Agent.fromJson(json['agent'] as Map<String, dynamic>),
    sector: json['sector'] == null
        ? null
        : Sector.fromJson(json['sector'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AgentSectorToJson(AgentSector instance) =>
    <String, dynamic>{
      'endDate': instance.endDate,
      'agentSectorPK': instance.agentSectorPK,
      'agent': instance.agent,
      'sector': instance.sector,
    };
