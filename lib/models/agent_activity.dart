
import 'dart:core';
import 'package:gos/models/agent.dart';
import 'package:gos/models/documents.dart';
import 'package:json_annotation/json_annotation.dart';

part 'agent_activity.g.dart';

@JsonSerializable()
class AgentActivity{
  AgentActivity({this.agentActivityId, this.agentAction, this.agentActivityDate, this.status, this.agentId, this.documentId});

  int? agentActivityId;
  String? agentAction;
  String? agentActivityDate;
  int? status;
  Agent? agentId;
  Documents? documentId;

  factory AgentActivity.fromJson(Map<String, dynamic> json) => _$AgentActivityFromJson(json);
  Map<String, dynamic> toJson() => _$AgentActivityToJson(this);

}