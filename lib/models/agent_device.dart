import 'dart:core';
import 'package:gos/models/agent.dart';
import 'package:gos/models/agent_device_pk.dart';
import 'package:gos/models/device.dart';
import 'package:json_annotation/json_annotation.dart';

part 'agent_device.g.dart';

@JsonSerializable()
class AgentDevice{
  AgentDevice({this.endDate, this.agentDevicePK, this.agent, this.device});

  String? endDate;
  AgentDevicePK? agentDevicePK;
  Agent? agent;
  Device? device;

  factory AgentDevice.fromJson(Map<String, dynamic> json) => _$AgentDeviceFromJson(json);
  Map<String, dynamic> toJson() => _$AgentDeviceToJson(this);

}