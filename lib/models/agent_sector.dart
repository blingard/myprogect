import 'dart:core';
import 'package:gos/models/agent.dart';
import 'package:gos/models/agent_sector_pk.dart';
import 'package:gos/models/sector.dart';
import 'package:json_annotation/json_annotation.dart';

part 'agent_sector.g.dart';

@JsonSerializable()
class AgentSector{
  AgentSector({this.endDate, this.agentSectorPK, this.agent, this.sector});

  String? endDate;
  AgentSectorPK? agentSectorPK;
  Agent? agent;
  Sector? sector;

  factory AgentSector.fromJson(Map<String, dynamic> json) => _$AgentSectorFromJson(json);
  Map<String, dynamic> toJson() => _$AgentSectorToJson(this);

}