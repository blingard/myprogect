import 'dart:core';
import 'package:gos/models/site.dart';
import 'package:json_annotation/json_annotation.dart';

part 'gps_coordinates.g.dart';

@JsonSerializable()
class GpsCoordinates {
  GpsCoordinates({this.gpsCoordId, this.pointName, required this.xcoord, required this.ycoord, this.siteId});

  int? gpsCoordId;
  String? pointName;
  double xcoord;
  double ycoord;
  Site? siteId;

  factory GpsCoordinates.fromJson(Map<String, dynamic> json) => _$GpsCoordinatesFromJson(json);
  Map<String, dynamic> toJson() => _$GpsCoordinatesToJson(this);

}