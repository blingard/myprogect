// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agent_device_pk.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AgentDevicePK _$AgentDevicePKFromJson(Map<String, dynamic> json) {
  return AgentDevicePK(
    agentId: json['agentId'] as int,
    deviceId: json['deviceId'] as int,
    beginDate: json['beginDate'] as String,
  );
}

Map<String, dynamic> _$AgentDevicePKToJson(AgentDevicePK instance) =>
    <String, dynamic>{
      'agentId': instance.agentId,
      'deviceId': instance.deviceId,
      'beginDate': instance.beginDate,
    };
