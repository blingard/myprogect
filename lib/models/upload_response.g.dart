// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'upload_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UploadResponse _$UploadResponseFromJson(Map<String, dynamic> json) {
  return UploadResponse(
    uploadLocation: json['uploadLocation'] as String,
    uploadDate: json['status'] as String,
  );
}

Map<String, dynamic> _$UploadResponseToJson(UploadResponse instance) => <String, dynamic>{
  'uploadLocation': instance.uploadLocation,
  'uploadDate': instance.uploadDate,
};