// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agent_device.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AgentDevice _$AgentDeviceFromJson(Map<String, dynamic> json) {
  return AgentDevice(
    endDate: json['endDate'] as String,
    agentDevicePK: json['agentDevicePK'] == null
        ? null
        : AgentDevicePK.fromJson(json['agentDevicePK'] as Map<String, dynamic>),
    agent: json['agent'] == null
        ? null
        : Agent.fromJson(json['agent'] as Map<String, dynamic>),
    device: json['device'] == null
        ? null
        : Device.fromJson(json['device'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AgentDeviceToJson(AgentDevice instance) =>
    <String, dynamic>{
      'endDate': instance.endDate,
      'agentDevicePK': instance.agentDevicePK,
      'agent': instance.agent,
      'device': instance.device,
    };
