// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agent_sector_pk.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AgentSectorPK _$AgentSectorPKFromJson(Map<String, dynamic> json) {
  return AgentSectorPK(
    agentId: json['agentId'] as int,
    sectorId: json['sectorId'] as int,
    beginDate: json['beginDate'] as String,
  );
}

Map<String, dynamic> _$AgentSectorPKToJson(AgentSectorPK instance) =>
    <String, dynamic>{
      'agentId': instance.agentId,
      'sectorId': instance.sectorId,
      'beginDate': instance.beginDate,
    };
