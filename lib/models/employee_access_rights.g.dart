// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee_access_rights.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmployeeAccessRights _$EmployeeAccessRightsFromJson(Map<String, dynamic> json) {
  return EmployeeAccessRights(
    employeeId: json['employeeId'] as int,
    groupId: json['groupId'] as int,
    accessRightId: json['accessRightId'] as int,
    accessRightName: json['accessRightName'] as String,
  );
}

Map<String, dynamic> _$EmployeeAccessRightsToJson(
        EmployeeAccessRights instance) =>
    <String, dynamic>{
      'employeeId': instance.employeeId,
      'groupId': instance.groupId,
      'accessRightId': instance.accessRightId,
      'accessRightName': instance.accessRightName,
    };
