// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agent_activity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AgentActivity _$AgentActivityFromJson(Map<String, dynamic> json) {
  return AgentActivity(
    agentActivityId: json['agentActivityId'] as int,
    agentAction: json['agentAction'] as String,
    agentActivityDate: json['agentActivityDate'] as String,
    status: json['status'] as int,
    agentId: json['agentId'] == null
        ? null
        : Agent.fromJson(json['agentId'] as Map<String, dynamic>),
    documentId: json['documentId'] == null
        ? null
        : Documents.fromJson(json['documentId'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AgentActivityToJson(AgentActivity instance) =>
    <String, dynamic>{
      'agentActivityId': instance.agentActivityId,
      'agentAction': instance.agentAction,
      'agentActivityDate': instance.agentActivityDate,
      'status': instance.status,
      'agentId': instance.agentId,
      'documentId': instance.documentId,
    };
