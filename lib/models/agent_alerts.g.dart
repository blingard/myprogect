// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agent_alerts.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AgentAlerts _$AgentAlertsFromJson(Map<String, dynamic> json) {
  return AgentAlerts(
    alertsPurpose: json['alertsPurpose'] as String,
    agentResponse: json['agentResponse'] as String,
    status: json['status'] as int,
    agentId: json['agentId'] == null
        ? null
        : Agent.fromJson(json['agentId'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AgentAlertsToJson(AgentAlerts instance) =>
    <String, dynamic>{
      'alertsPurpose': instance.alertsPurpose,
      'agentResponse': instance.agentResponse,
      'status': instance.status,
      'agentId': instance.agentId,
    };
