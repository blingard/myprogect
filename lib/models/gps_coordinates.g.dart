// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gps_coordinates.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GpsCoordinates _$GpsCoordinatesFromJson(Map<String, dynamic> json) {
  return GpsCoordinates(
    gpsCoordId: json['gpsCoordId'] as int,
    pointName: json['pointName'] as String,
    xcoord: (json['xcoord'] as num).toDouble(),
    ycoord: (json['ycoord'] as num).toDouble(),
    siteId: json['siteId'] as Site,
  );
}

Map<String, dynamic> _$GpsCoordinatesToJson(GpsCoordinates instance) =>
    <String, dynamic>{
      'gpsCoordId': instance.gpsCoordId,
      'pointName': instance.pointName,
      'xcoord': instance.xcoord,
      'ycoord': instance.ycoord,
      'siteId': instance.siteId,
    };
