import 'dart:core';
import 'package:json_annotation/json_annotation.dart';

part 'agent_sector_pk.g.dart';

@JsonSerializable()
class AgentSectorPK{
  AgentSectorPK({this.agentId, this.sectorId, this.beginDate});

  int? agentId;
  int? sectorId;
  String? beginDate;

  factory AgentSectorPK.fromJson(Map<String, dynamic> json) => _$AgentSectorPKFromJson(json);
  Map<String, dynamic> toJson() => _$AgentSectorPKToJson(this);

}