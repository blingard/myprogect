import 'dart:core';
import 'package:gos/models/agent.dart';
import 'package:json_annotation/json_annotation.dart';

part 'agent_alerts.g.dart';

@JsonSerializable()
class AgentAlerts{
  AgentAlerts({this.alertsPurpose, this.agentResponse, this.status, this.agentId});

  String? alertsPurpose;
  String? agentResponse;
  int? status;
  Agent? agentId;

  factory AgentAlerts.fromJson(Map<String, dynamic> json) => _$AgentAlertsFromJson(json);
  Map<String, dynamic> toJson() => _$AgentAlertsToJson(this);

}