// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Agent _$AgentFromJson(Map<String, dynamic> json) {
  return Agent(
    agentId: json['agentId'] as int,
    agentName: json['agentName'] as String,
    agentLogin: json['agentLogin'] as String,
    agentPassword: json['agentPassword'] as String,
    isFirstConnection: json['isFirstConnection'] as bool,
    status: json['status'] as int,
    agentPhoneNumber: json['agentPhoneNumber'] as String,
    agentDeviceList: json['agentDeviceList'] as List<AgentDevice>,
    agentSectorList: json['agentSectorList'] as List<AgentSector>,
  );
}

Map<String, dynamic> _$AgentToJson(Agent instance) => <String, dynamic>{
      'agentId': instance.agentId,
      'agentName': instance.agentName,
      'agentLogin': instance.agentLogin,
      'agentPassword': instance.agentPassword,
      'isFirstConnection': instance.isFirstConnection,
      'status': instance.status,
      'agentPhoneNumber': instance.agentPhoneNumber,
      'agentDeviceList': instance.agentDeviceList,
      'agentSectorList': instance.agentSectorList,
    };
