import 'dart:core';
import 'package:gos/models/agent_sector.dart';
import 'package:gos/models/district.dart';
import 'package:gos/models/site.dart';
import 'package:json_annotation/json_annotation.dart';

part 'sector.g.dart';

@JsonSerializable()
class Sector{
  Sector({required this.sectorId, required this.sectorName, required this.description, required this.status, required this.agentSectorList, required this.districtList});

  int sectorId;
  String sectorName;
  String description;
  int status;
  List<AgentSector> agentSectorList;
  List<District> districtList;

  factory Sector.fromJson(Map<String, dynamic> json) => _$SectorFromJson(json);
  Map<String, dynamic> toJson() => _$SectorToJson(this);

}