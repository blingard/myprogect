import 'dart:core';
import 'package:gos/models/doctype.dart';
import 'package:gos/models/site.dart';
import 'package:json_annotation/json_annotation.dart';

part 'documents.g.dart';

@JsonSerializable()
class Documents{
  Documents({required this.documentId, required this.documentName, required this.documentPath, required this.secretKey, required this.documentExtension, required this.createdDate, required this.status
    , required this.docTypeId, required this.siteId, required this.documentCode, required this.documentNumber});

  int? documentId;
  String? documentName;
  String? documentCode;
  String? documentNumber;
  String? documentPath;
  String? secretKey;
  String? documentExtension;
  String? createdDate;
  int? status;
  DocType? docTypeId;
  Site? siteId;

  factory Documents.fromJson(Map<String, dynamic> json) => _$DocumentsFromJson(json);
  Map<String, dynamic> toJson() => _$DocumentsToJson(this);

}