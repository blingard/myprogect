import 'dart:core';
import 'package:gos/models/sector.dart';
import 'package:gos/models/site.dart';
import 'package:json_annotation/json_annotation.dart';

part "district.g.dart";

@JsonSerializable()
class District{
  District({required this.districtId, required this.districtName, required this.description, required this.commune, required this.standing, required this.population, required this.densite, required this.status, required this.siteList, this.sectorId});

  int districtId;
  String districtName;
  String description;
  String commune;
  String standing;
  String population;
  String densite;
  int status;
  List<Site> siteList;
  Sector? sectorId;

  factory District.fromJson(Map<String, dynamic> json) => _$DistrictFromJson(json);
  Map<String, dynamic> toJson() => _$DistrictToJson(this);

}