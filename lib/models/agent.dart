import 'dart:core';
import 'package:gos/models/agent_device.dart';
import 'package:gos/models/agent_sector.dart';
import 'package:json_annotation/json_annotation.dart';
part 'agent.g.dart';

@JsonSerializable()
class Agent{

  Agent({required this.agentId, required this.agentName, required this.agentLogin, required this.agentPassword, required this.isFirstConnection, required this.status, required this.agentPhoneNumber, required this.agentDeviceList,
    required this.agentSectorList});

  int? agentId;
  String? agentName;
  String agentLogin;
  String? agentPassword;
  bool? isFirstConnection;
  int? status;
  String? agentPhoneNumber;
  List<AgentDevice>? agentDeviceList;
  List<AgentSector>? agentSectorList;

  factory Agent.fromJson(Map<String, dynamic> json) => _$AgentFromJson(json);
  Map<String, dynamic> toJson() => _$AgentToJson(this);
}