// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sector.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sector _$SectorFromJson(Map<String, dynamic> json) {
  return Sector(
    sectorId: json['sectorId'] as int,
    sectorName: json['sectorName'] as String,
    description: json['description'] as String,
    status: json['status'] as int,
    districtList: json['districtList'] as List<District>,
    agentSectorList: json['agentSectorList'] as List<AgentSector>,
  );
}

Map<String, dynamic> _$SectorToJson(Sector instance) => <String, dynamic>{
      'sectorId': instance.sectorId,
      'sectorName': instance.sectorName,
      'description': instance.description,
      'status': instance.status,
      'districtList': instance.districtList,
      'agentSectorList': instance.agentSectorList,
    };
