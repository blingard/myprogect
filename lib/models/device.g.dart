// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Device _$DeviceFromJson(Map<String, dynamic> json) {
  return Device(
    deviceId: json['deviceId'] as int,
    deviceName: json['deviceName'] as String,
    status: json['status'] as int,
    agentDeviceList: json['agentDeviceList'] as List<AgentDevice>,
  );
}

Map<String, dynamic> _$DeviceToJson(Device instance) => <String, dynamic>{
      'deviceId': instance.deviceId,
      'deviceName': instance.deviceName,
      'status': instance.status,
      'agentDeviceList': instance.agentDeviceList,
    };
