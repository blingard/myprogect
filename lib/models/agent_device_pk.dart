import 'dart:core';
import 'package:json_annotation/json_annotation.dart';

part 'agent_device_pk.g.dart';

@JsonSerializable()
class AgentDevicePK{
  AgentDevicePK({this.agentId, this.deviceId, this.beginDate});

  int? agentId;
  int? deviceId;
  String? beginDate;

  factory AgentDevicePK.fromJson(Map<String, dynamic> json) => _$AgentDevicePKFromJson(json);
  Map<String, dynamic> toJson() => _$AgentDevicePKToJson(this);

}