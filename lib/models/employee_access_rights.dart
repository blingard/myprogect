import 'dart:core';
import 'package:json_annotation/json_annotation.dart';

part 'employee_access_rights.g.dart';

@JsonSerializable()
class EmployeeAccessRights{
  EmployeeAccessRights({this.employeeId, this.groupId, this.accessRightId, this.accessRightName});

  int? employeeId;
  int? groupId;
  int? accessRightId;
  String? accessRightName;

  factory EmployeeAccessRights.fromJson(Map<String, dynamic> json) => _$EmployeeAccessRightsFromJson(json);
  Map<String, dynamic> toJson() => _$EmployeeAccessRightsToJson(this);

}