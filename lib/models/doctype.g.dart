// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'doctype.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DocType _$DocTypeFromJson(Map<String, dynamic> json) {
  return DocType(
    docTypeId: json['docTypeId'] as int,
    docTypeName: json['docTypeName'] as String,
    description: json['description'] as String,
    documentsList: json['documentsList'] as List<Documents>,
  );
}

Map<String, dynamic> _$DocTypeToJson(DocType instance) => <String, dynamic>{
      'docTypeId': instance.docTypeId,
      'docTypeName': instance.docTypeName,
      'description': instance.description,
      'documentsList': instance.documentsList,
    };
