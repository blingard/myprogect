// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'district.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

District _$DistrictFromJson(Map<String, dynamic> json) {
  return District(
    districtId: json['districtId'] as int,
    districtName: json['districtName'] as String,
    description: json['description'] as String,
    commune: json['commune'] as String,
    standing: json['standing'] as String,
    population: json['population'] as String,
    densite: json['densite'] as String,
    status: json['status'] as int,
    siteList: json['siteList'] as List<Site>,
    sectorId: json['sectorId'] as Sector,
  );
}

Map<String, dynamic> _$DistrictToJson(District instance) => <String, dynamic>{
  'districtId': instance.districtId,
  'districtName': instance.districtName,
  'description': instance.description,
  'status': instance.status,
  'siteList': instance.siteList,
  'sectorId': instance.sectorId,
  'commune': instance.commune,
  'standing': instance.standing,
  'population': instance.population,
  'densite': instance.densite,
};
