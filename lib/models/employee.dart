import 'dart:core';
import 'package:gos/models/employee_access_rights.dart';
import 'package:json_annotation/json_annotation.dart';

part 'employee.g.dart';

@JsonSerializable()
class Employee{
  Employee({this.employeeId, this.employeeName,this.employeeSurname, this.employeeEmail,
            this.employeePassword, this.employeePhoneNumber,this.employeeMatricule, this.employeeLogin,
            this.employeeBirthdate, this.employeePlaceOfBirth,this.employeeNationality, this.employeeAdress,
            this.isFirstConnection, this.employeeGender,this.status, this.employeeGroupEmployeeList, this.notificationList});

  int? employeeId;
  String? employeeName;
  String? employeeSurname;
  String? employeeEmail;
  String? employeePassword;
  String? employeePhoneNumber;
  String? employeeMatricule;
  String? employeeLogin;
  String? employeeBirthdate;
  String? employeePlaceOfBirth;
  String? employeeNationality;
  String? employeeAdress;
  bool? isFirstConnection;
  String? employeeGender;
  int? status;
  EmployeeAccessRights? employeeGroupEmployeeList;
  String? notificationList;


  factory Employee.fromJson(Map<String, dynamic> json) => _$EmployeeFromJson(json);
  Map<String, dynamic> toJson() => _$EmployeeToJson(this);


  /*
  "employee":{"notificationList":null}
  *
  *
  *
  *
  * */

}