import 'dart:core';
import 'package:gos/models/agent_device.dart';
import 'package:json_annotation/json_annotation.dart';

part 'device.g.dart';

@JsonSerializable()
class Device{
  Device({this.deviceId, this.deviceName, this.status, this.agentDeviceList});

  int? deviceId;
  String? deviceName;
  int? status;
  List<AgentDevice>? agentDeviceList;

  factory Device.fromJson(Map<String, dynamic> json) => _$DeviceFromJson(json);
  Map<String, dynamic> toJson() => _$DeviceToJson(this);

}