import 'dart:core';
import 'package:json_annotation/json_annotation.dart';
part 'upload_response.g.dart';

@JsonSerializable()
class UploadResponse{

  UploadResponse({this.uploadLocation, this.uploadDate});

  String? uploadLocation;
  String? uploadDate;

  factory UploadResponse.fromJson(Map<String, dynamic> json) => _$UploadResponseFromJson(json);
  Map<String, dynamic> toJson() => _$UploadResponseToJson(this);
}