import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:gos/service/http_service.dart';
//import 'package:form_field_validator/form_field_validator.dart';
final locator = GetIt.instance;
void setup(){
  locator.registerSingleton<HTTPService>(HTTPService());
}
const primaryColor = Color(0xFF00BF6D);
const secondaryColor = Color(0xFFFE9901);
const contentColorLightTheme = Color(0xFF1D1D35);
const contentColorDarkTheme = Color(0xFFF5FCF9);
const warninngColor = Color(0xFFF3BB1C);
const errorColor = Color(0xFFF03738);

const defaultPadding = 16.0;

const logoDarkTheme = "assets/icons/Only_logo_dark_theme.svg";
const logoLightTheme = "assets/icons/Only_logo_light_theme.svg";

const requiredField = "This field is required";
const invalidEmail = "Enter a valid email address";
const duree = Duration(milliseconds: 1500);
/*final passwordValidator = MultiValidator(
  [
    RequiredValidator(errorText: requiredField),
    MinLengthValidator(8, errorText: 'password must be at least 8 digits long'),
    PatternValidator(r'(?=.*?[#?!@$%^&*-])',
        errorText: 'passwords must have at least one special character')
  ],
);*/
final patter =  r"[6]{1}[0-9]{8}$";
final RegExp emailValidatorRegExp =
RegExp(patter);
const String kLoginNullErrorEn = "Please Enter your login";
const String kPassNullErrorEn = "Please Enter your password";
const String kPhoneNumberNullErrorEn = "Please Enter your phone number";
const String kAddressNullErrorEn = "Please Enter your address";
const String kLoginNullErrorFr = "Saisir votre nom d'utilisateur";
const String kPassNullErrorFr = "Saisir votre mot de passe";
const String kPhoneNumberNullErrorFr = "Saisir votre numero de téléphone";
const String kAddressNullErrorFr = "Saisir votre adresse";

const InputDecoration otpInputDecoration = InputDecoration(
  filled: false,
  border: UnderlineInputBorder(),
  hintText: "0",
);
